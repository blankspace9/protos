//
//  Generated code. Do not modify.
//  source: mis/mis.proto
//
// @dart = 2.12

// ignore_for_file: annotate_overrides, camel_case_types, comment_references
// ignore_for_file: constant_identifier_names
// ignore_for_file: deprecated_member_use_from_same_package, library_prefixes
// ignore_for_file: non_constant_identifier_names, prefer_final_fields
// ignore_for_file: unnecessary_import, unnecessary_this, unused_import

import 'dart:async' as $async;
import 'dart:core' as $core;

import 'package:protobuf/protobuf.dart' as $pb;

import 'mis.pb.dart' as $1;
import 'mis.pbjson.dart';

export 'mis.pb.dart';

abstract class MISServiceBase extends $pb.GeneratedService {
  $async.Future<$1.CreateResponse> registerPatient($pb.ServerContext ctx, $1.PatientRequest request);
  $async.Future<$1.CreateResponse> registerDoctor($pb.ServerContext ctx, $1.DoctorRequest request);
  $async.Future<$1.CreateResponse> registerAdmin($pb.ServerContext ctx, $1.AdminRequest request);
  $async.Future<$1.LoginResponse> login($pb.ServerContext ctx, $1.LoginRequest request);
  $async.Future<$1.PatientResponse> getPatientByEmail($pb.ServerContext ctx, $1.EmailRequest request);
  $async.Future<$1.PatientResponse> getPatientByID($pb.ServerContext ctx, $1.IDRequest request);
  $async.Future<$1.PrescriptionList> getPatientPrescriptions($pb.ServerContext ctx, $1.IDRequest request);
  $async.Future<$1.DoctorList> getPatientDoctors($pb.ServerContext ctx, $1.IDRequest request);
  $async.Future<$1.SuccessResponse> deletePatient($pb.ServerContext ctx, $1.IDRequest request);
  $async.Future<$1.DoctorResponse> getDoctorByEmail($pb.ServerContext ctx, $1.EmailRequest request);
  $async.Future<$1.DoctorResponse> getDoctorByID($pb.ServerContext ctx, $1.IDRequest request);
  $async.Future<$1.SuccessResponse> addPatient($pb.ServerContext ctx, $1.EmailRequest request);
  $async.Future<$1.PrescriptionList> getDoctorPrescriptions($pb.ServerContext ctx, $1.IDRequest request);
  $async.Future<$1.PatientList> getDoctorPatients($pb.ServerContext ctx, $1.IDRequest request);
  $async.Future<$1.PatientList> getDoctorPatientsWithPrescriptions($pb.ServerContext ctx, $1.IDRequest request);
  $async.Future<$1.SuccessResponse> deleteDoctor($pb.ServerContext ctx, $1.IDRequest request);
  $async.Future<$1.CreateResponse> addPrescription($pb.ServerContext ctx, $1.PrescriptionRequest request);
  $async.Future<$1.PrescriptionResponse> getPrescriptionByID($pb.ServerContext ctx, $1.IDRequest request);
  $async.Future<$1.PrescriptionList> getPrescriptionsByBoth($pb.ServerContext ctx, $1.PatientDoctorIDRequest request);
  $async.Future<$1.SuccessResponse> deletePrescription($pb.ServerContext ctx, $1.IDRequest request);
  $async.Future<$1.SuccessResponse> updatePrescription($pb.ServerContext ctx, $1.PrescriptionUpdateRequest request);

  $pb.GeneratedMessage createRequest($core.String methodName) {
    switch (methodName) {
      case 'RegisterPatient': return $1.PatientRequest();
      case 'RegisterDoctor': return $1.DoctorRequest();
      case 'RegisterAdmin': return $1.AdminRequest();
      case 'Login': return $1.LoginRequest();
      case 'GetPatientByEmail': return $1.EmailRequest();
      case 'GetPatientByID': return $1.IDRequest();
      case 'GetPatientPrescriptions': return $1.IDRequest();
      case 'GetPatientDoctors': return $1.IDRequest();
      case 'DeletePatient': return $1.IDRequest();
      case 'GetDoctorByEmail': return $1.EmailRequest();
      case 'GetDoctorByID': return $1.IDRequest();
      case 'AddPatient': return $1.EmailRequest();
      case 'GetDoctorPrescriptions': return $1.IDRequest();
      case 'GetDoctorPatients': return $1.IDRequest();
      case 'GetDoctorPatientsWithPrescriptions': return $1.IDRequest();
      case 'DeleteDoctor': return $1.IDRequest();
      case 'AddPrescription': return $1.PrescriptionRequest();
      case 'GetPrescriptionByID': return $1.IDRequest();
      case 'GetPrescriptionsByBoth': return $1.PatientDoctorIDRequest();
      case 'DeletePrescription': return $1.IDRequest();
      case 'UpdatePrescription': return $1.PrescriptionUpdateRequest();
      default: throw $core.ArgumentError('Unknown method: $methodName');
    }
  }

  $async.Future<$pb.GeneratedMessage> handleCall($pb.ServerContext ctx, $core.String methodName, $pb.GeneratedMessage request) {
    switch (methodName) {
      case 'RegisterPatient': return this.registerPatient(ctx, request as $1.PatientRequest);
      case 'RegisterDoctor': return this.registerDoctor(ctx, request as $1.DoctorRequest);
      case 'RegisterAdmin': return this.registerAdmin(ctx, request as $1.AdminRequest);
      case 'Login': return this.login(ctx, request as $1.LoginRequest);
      case 'GetPatientByEmail': return this.getPatientByEmail(ctx, request as $1.EmailRequest);
      case 'GetPatientByID': return this.getPatientByID(ctx, request as $1.IDRequest);
      case 'GetPatientPrescriptions': return this.getPatientPrescriptions(ctx, request as $1.IDRequest);
      case 'GetPatientDoctors': return this.getPatientDoctors(ctx, request as $1.IDRequest);
      case 'DeletePatient': return this.deletePatient(ctx, request as $1.IDRequest);
      case 'GetDoctorByEmail': return this.getDoctorByEmail(ctx, request as $1.EmailRequest);
      case 'GetDoctorByID': return this.getDoctorByID(ctx, request as $1.IDRequest);
      case 'AddPatient': return this.addPatient(ctx, request as $1.EmailRequest);
      case 'GetDoctorPrescriptions': return this.getDoctorPrescriptions(ctx, request as $1.IDRequest);
      case 'GetDoctorPatients': return this.getDoctorPatients(ctx, request as $1.IDRequest);
      case 'GetDoctorPatientsWithPrescriptions': return this.getDoctorPatientsWithPrescriptions(ctx, request as $1.IDRequest);
      case 'DeleteDoctor': return this.deleteDoctor(ctx, request as $1.IDRequest);
      case 'AddPrescription': return this.addPrescription(ctx, request as $1.PrescriptionRequest);
      case 'GetPrescriptionByID': return this.getPrescriptionByID(ctx, request as $1.IDRequest);
      case 'GetPrescriptionsByBoth': return this.getPrescriptionsByBoth(ctx, request as $1.PatientDoctorIDRequest);
      case 'DeletePrescription': return this.deletePrescription(ctx, request as $1.IDRequest);
      case 'UpdatePrescription': return this.updatePrescription(ctx, request as $1.PrescriptionUpdateRequest);
      default: throw $core.ArgumentError('Unknown method: $methodName');
    }
  }

  $core.Map<$core.String, $core.dynamic> get $json => MISServiceBase$json;
  $core.Map<$core.String, $core.Map<$core.String, $core.dynamic>> get $messageJson => MISServiceBase$messageJson;
}

abstract class ChatServiceBase extends $pb.GeneratedService {
  $async.Future<$1.SuccessResponse> sendMessage($pb.ServerContext ctx, $1.Message request);
  $async.Future<$1.Message> receiveMessages($pb.ServerContext ctx, $1.JoinSessionRequest request);
  $async.Future<$1.Message> joinChannel($pb.ServerContext ctx, $1.JoinSessionRequest request);
  $async.Future<$1.SuccessResponse> sendMsg($pb.ServerContext ctx, $1.Message request);

  $pb.GeneratedMessage createRequest($core.String methodName) {
    switch (methodName) {
      case 'SendMessage': return $1.Message();
      case 'ReceiveMessages': return $1.JoinSessionRequest();
      case 'JoinChannel': return $1.JoinSessionRequest();
      case 'SendMsg': return $1.Message();
      default: throw $core.ArgumentError('Unknown method: $methodName');
    }
  }

  $async.Future<$pb.GeneratedMessage> handleCall($pb.ServerContext ctx, $core.String methodName, $pb.GeneratedMessage request) {
    switch (methodName) {
      case 'SendMessage': return this.sendMessage(ctx, request as $1.Message);
      case 'ReceiveMessages': return this.receiveMessages(ctx, request as $1.JoinSessionRequest);
      case 'JoinChannel': return this.joinChannel(ctx, request as $1.JoinSessionRequest);
      case 'SendMsg': return this.sendMsg(ctx, request as $1.Message);
      default: throw $core.ArgumentError('Unknown method: $methodName');
    }
  }

  $core.Map<$core.String, $core.dynamic> get $json => ChatServiceBase$json;
  $core.Map<$core.String, $core.Map<$core.String, $core.dynamic>> get $messageJson => ChatServiceBase$messageJson;
}

