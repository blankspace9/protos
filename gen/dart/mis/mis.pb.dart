//
//  Generated code. Do not modify.
//  source: mis/mis.proto
//
// @dart = 2.12

// ignore_for_file: annotate_overrides, camel_case_types, comment_references
// ignore_for_file: constant_identifier_names, library_prefixes
// ignore_for_file: non_constant_identifier_names, prefer_final_fields
// ignore_for_file: unnecessary_import, unnecessary_this, unused_import

import 'dart:async' as $async;
import 'dart:core' as $core;

import 'package:fixnum/fixnum.dart' as $fixnum;
import 'package:protobuf/protobuf.dart' as $pb;

import '../google/protobuf/timestamp.pb.dart' as $0;

/// MIS messages
class PatientRequest extends $pb.GeneratedMessage {
  factory PatientRequest({
    $core.String? email,
    $core.String? password,
    $core.String? firstName,
    $core.String? lastName,
    $core.String? patronymic,
    $core.String? phoneNumber,
  }) {
    final $result = create();
    if (email != null) {
      $result.email = email;
    }
    if (password != null) {
      $result.password = password;
    }
    if (firstName != null) {
      $result.firstName = firstName;
    }
    if (lastName != null) {
      $result.lastName = lastName;
    }
    if (patronymic != null) {
      $result.patronymic = patronymic;
    }
    if (phoneNumber != null) {
      $result.phoneNumber = phoneNumber;
    }
    return $result;
  }
  PatientRequest._() : super();
  factory PatientRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PatientRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(_omitMessageNames ? '' : 'PatientRequest', package: const $pb.PackageName(_omitMessageNames ? '' : 'mis'), createEmptyInstance: create)
    ..aOS(1, _omitFieldNames ? '' : 'email')
    ..aOS(2, _omitFieldNames ? '' : 'password')
    ..aOS(3, _omitFieldNames ? '' : 'firstName')
    ..aOS(4, _omitFieldNames ? '' : 'lastName')
    ..aOS(5, _omitFieldNames ? '' : 'patronymic')
    ..aOS(6, _omitFieldNames ? '' : 'phoneNumber')
    ..hasRequiredFields = false
  ;

  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PatientRequest clone() => PatientRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PatientRequest copyWith(void Function(PatientRequest) updates) => super.copyWith((message) => updates(message as PatientRequest)) as PatientRequest;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static PatientRequest create() => PatientRequest._();
  PatientRequest createEmptyInstance() => create();
  static $pb.PbList<PatientRequest> createRepeated() => $pb.PbList<PatientRequest>();
  @$core.pragma('dart2js:noInline')
  static PatientRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PatientRequest>(create);
  static PatientRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get email => $_getSZ(0);
  @$pb.TagNumber(1)
  set email($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasEmail() => $_has(0);
  @$pb.TagNumber(1)
  void clearEmail() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get password => $_getSZ(1);
  @$pb.TagNumber(2)
  set password($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPassword() => $_has(1);
  @$pb.TagNumber(2)
  void clearPassword() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get firstName => $_getSZ(2);
  @$pb.TagNumber(3)
  set firstName($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasFirstName() => $_has(2);
  @$pb.TagNumber(3)
  void clearFirstName() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get lastName => $_getSZ(3);
  @$pb.TagNumber(4)
  set lastName($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasLastName() => $_has(3);
  @$pb.TagNumber(4)
  void clearLastName() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get patronymic => $_getSZ(4);
  @$pb.TagNumber(5)
  set patronymic($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasPatronymic() => $_has(4);
  @$pb.TagNumber(5)
  void clearPatronymic() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get phoneNumber => $_getSZ(5);
  @$pb.TagNumber(6)
  set phoneNumber($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasPhoneNumber() => $_has(5);
  @$pb.TagNumber(6)
  void clearPhoneNumber() => clearField(6);
}

class DoctorRequest extends $pb.GeneratedMessage {
  factory DoctorRequest({
    $core.String? email,
    $core.String? password,
    $core.String? firstName,
    $core.String? lastName,
    $core.String? patronymic,
    $core.String? phoneNumber,
    $core.String? specialty,
    $core.String? license,
  }) {
    final $result = create();
    if (email != null) {
      $result.email = email;
    }
    if (password != null) {
      $result.password = password;
    }
    if (firstName != null) {
      $result.firstName = firstName;
    }
    if (lastName != null) {
      $result.lastName = lastName;
    }
    if (patronymic != null) {
      $result.patronymic = patronymic;
    }
    if (phoneNumber != null) {
      $result.phoneNumber = phoneNumber;
    }
    if (specialty != null) {
      $result.specialty = specialty;
    }
    if (license != null) {
      $result.license = license;
    }
    return $result;
  }
  DoctorRequest._() : super();
  factory DoctorRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory DoctorRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(_omitMessageNames ? '' : 'DoctorRequest', package: const $pb.PackageName(_omitMessageNames ? '' : 'mis'), createEmptyInstance: create)
    ..aOS(1, _omitFieldNames ? '' : 'email')
    ..aOS(2, _omitFieldNames ? '' : 'password')
    ..aOS(3, _omitFieldNames ? '' : 'firstName')
    ..aOS(4, _omitFieldNames ? '' : 'lastName')
    ..aOS(5, _omitFieldNames ? '' : 'patronymic')
    ..aOS(6, _omitFieldNames ? '' : 'phoneNumber')
    ..aOS(7, _omitFieldNames ? '' : 'specialty')
    ..aOS(8, _omitFieldNames ? '' : 'license')
    ..hasRequiredFields = false
  ;

  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  DoctorRequest clone() => DoctorRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  DoctorRequest copyWith(void Function(DoctorRequest) updates) => super.copyWith((message) => updates(message as DoctorRequest)) as DoctorRequest;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static DoctorRequest create() => DoctorRequest._();
  DoctorRequest createEmptyInstance() => create();
  static $pb.PbList<DoctorRequest> createRepeated() => $pb.PbList<DoctorRequest>();
  @$core.pragma('dart2js:noInline')
  static DoctorRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<DoctorRequest>(create);
  static DoctorRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get email => $_getSZ(0);
  @$pb.TagNumber(1)
  set email($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasEmail() => $_has(0);
  @$pb.TagNumber(1)
  void clearEmail() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get password => $_getSZ(1);
  @$pb.TagNumber(2)
  set password($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPassword() => $_has(1);
  @$pb.TagNumber(2)
  void clearPassword() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get firstName => $_getSZ(2);
  @$pb.TagNumber(3)
  set firstName($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasFirstName() => $_has(2);
  @$pb.TagNumber(3)
  void clearFirstName() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get lastName => $_getSZ(3);
  @$pb.TagNumber(4)
  set lastName($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasLastName() => $_has(3);
  @$pb.TagNumber(4)
  void clearLastName() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get patronymic => $_getSZ(4);
  @$pb.TagNumber(5)
  set patronymic($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasPatronymic() => $_has(4);
  @$pb.TagNumber(5)
  void clearPatronymic() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get phoneNumber => $_getSZ(5);
  @$pb.TagNumber(6)
  set phoneNumber($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasPhoneNumber() => $_has(5);
  @$pb.TagNumber(6)
  void clearPhoneNumber() => clearField(6);

  @$pb.TagNumber(7)
  $core.String get specialty => $_getSZ(6);
  @$pb.TagNumber(7)
  set specialty($core.String v) { $_setString(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasSpecialty() => $_has(6);
  @$pb.TagNumber(7)
  void clearSpecialty() => clearField(7);

  @$pb.TagNumber(8)
  $core.String get license => $_getSZ(7);
  @$pb.TagNumber(8)
  set license($core.String v) { $_setString(7, v); }
  @$pb.TagNumber(8)
  $core.bool hasLicense() => $_has(7);
  @$pb.TagNumber(8)
  void clearLicense() => clearField(8);
}

class AdminRequest extends $pb.GeneratedMessage {
  factory AdminRequest({
    $core.String? email,
    $core.String? password,
  }) {
    final $result = create();
    if (email != null) {
      $result.email = email;
    }
    if (password != null) {
      $result.password = password;
    }
    return $result;
  }
  AdminRequest._() : super();
  factory AdminRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory AdminRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(_omitMessageNames ? '' : 'AdminRequest', package: const $pb.PackageName(_omitMessageNames ? '' : 'mis'), createEmptyInstance: create)
    ..aOS(1, _omitFieldNames ? '' : 'email')
    ..aOS(2, _omitFieldNames ? '' : 'password')
    ..hasRequiredFields = false
  ;

  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  AdminRequest clone() => AdminRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  AdminRequest copyWith(void Function(AdminRequest) updates) => super.copyWith((message) => updates(message as AdminRequest)) as AdminRequest;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static AdminRequest create() => AdminRequest._();
  AdminRequest createEmptyInstance() => create();
  static $pb.PbList<AdminRequest> createRepeated() => $pb.PbList<AdminRequest>();
  @$core.pragma('dart2js:noInline')
  static AdminRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<AdminRequest>(create);
  static AdminRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get email => $_getSZ(0);
  @$pb.TagNumber(1)
  set email($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasEmail() => $_has(0);
  @$pb.TagNumber(1)
  void clearEmail() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get password => $_getSZ(1);
  @$pb.TagNumber(2)
  set password($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPassword() => $_has(1);
  @$pb.TagNumber(2)
  void clearPassword() => clearField(2);
}

class PrescriptionRequest extends $pb.GeneratedMessage {
  factory PrescriptionRequest({
    $fixnum.Int64? patientId,
    $fixnum.Int64? doctorId,
    $core.String? dateOfIssue,
    $fixnum.Int64? durationDays,
    $core.String? title,
    $fixnum.Int64? dosageMg,
    $core.String? wayToUse,
  }) {
    final $result = create();
    if (patientId != null) {
      $result.patientId = patientId;
    }
    if (doctorId != null) {
      $result.doctorId = doctorId;
    }
    if (dateOfIssue != null) {
      $result.dateOfIssue = dateOfIssue;
    }
    if (durationDays != null) {
      $result.durationDays = durationDays;
    }
    if (title != null) {
      $result.title = title;
    }
    if (dosageMg != null) {
      $result.dosageMg = dosageMg;
    }
    if (wayToUse != null) {
      $result.wayToUse = wayToUse;
    }
    return $result;
  }
  PrescriptionRequest._() : super();
  factory PrescriptionRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PrescriptionRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(_omitMessageNames ? '' : 'PrescriptionRequest', package: const $pb.PackageName(_omitMessageNames ? '' : 'mis'), createEmptyInstance: create)
    ..aInt64(1, _omitFieldNames ? '' : 'patientId')
    ..aInt64(2, _omitFieldNames ? '' : 'doctorId')
    ..aOS(3, _omitFieldNames ? '' : 'dateOfIssue')
    ..aInt64(4, _omitFieldNames ? '' : 'durationDays')
    ..aOS(5, _omitFieldNames ? '' : 'title')
    ..aInt64(6, _omitFieldNames ? '' : 'dosageMg')
    ..aOS(7, _omitFieldNames ? '' : 'wayToUse')
    ..hasRequiredFields = false
  ;

  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PrescriptionRequest clone() => PrescriptionRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PrescriptionRequest copyWith(void Function(PrescriptionRequest) updates) => super.copyWith((message) => updates(message as PrescriptionRequest)) as PrescriptionRequest;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static PrescriptionRequest create() => PrescriptionRequest._();
  PrescriptionRequest createEmptyInstance() => create();
  static $pb.PbList<PrescriptionRequest> createRepeated() => $pb.PbList<PrescriptionRequest>();
  @$core.pragma('dart2js:noInline')
  static PrescriptionRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PrescriptionRequest>(create);
  static PrescriptionRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get patientId => $_getI64(0);
  @$pb.TagNumber(1)
  set patientId($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPatientId() => $_has(0);
  @$pb.TagNumber(1)
  void clearPatientId() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get doctorId => $_getI64(1);
  @$pb.TagNumber(2)
  set doctorId($fixnum.Int64 v) { $_setInt64(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasDoctorId() => $_has(1);
  @$pb.TagNumber(2)
  void clearDoctorId() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get dateOfIssue => $_getSZ(2);
  @$pb.TagNumber(3)
  set dateOfIssue($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasDateOfIssue() => $_has(2);
  @$pb.TagNumber(3)
  void clearDateOfIssue() => clearField(3);

  @$pb.TagNumber(4)
  $fixnum.Int64 get durationDays => $_getI64(3);
  @$pb.TagNumber(4)
  set durationDays($fixnum.Int64 v) { $_setInt64(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasDurationDays() => $_has(3);
  @$pb.TagNumber(4)
  void clearDurationDays() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get title => $_getSZ(4);
  @$pb.TagNumber(5)
  set title($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasTitle() => $_has(4);
  @$pb.TagNumber(5)
  void clearTitle() => clearField(5);

  @$pb.TagNumber(6)
  $fixnum.Int64 get dosageMg => $_getI64(5);
  @$pb.TagNumber(6)
  set dosageMg($fixnum.Int64 v) { $_setInt64(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasDosageMg() => $_has(5);
  @$pb.TagNumber(6)
  void clearDosageMg() => clearField(6);

  @$pb.TagNumber(7)
  $core.String get wayToUse => $_getSZ(6);
  @$pb.TagNumber(7)
  set wayToUse($core.String v) { $_setString(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasWayToUse() => $_has(6);
  @$pb.TagNumber(7)
  void clearWayToUse() => clearField(7);
}

class PrescriptionUpdateRequest extends $pb.GeneratedMessage {
  factory PrescriptionUpdateRequest({
    $fixnum.Int64? id,
    $fixnum.Int64? patientId,
    $fixnum.Int64? doctorId,
    $core.String? dateOfIssue,
    $fixnum.Int64? durationDays,
    $core.String? title,
    $fixnum.Int64? dosageMg,
    $core.String? wayToUse,
  }) {
    final $result = create();
    if (id != null) {
      $result.id = id;
    }
    if (patientId != null) {
      $result.patientId = patientId;
    }
    if (doctorId != null) {
      $result.doctorId = doctorId;
    }
    if (dateOfIssue != null) {
      $result.dateOfIssue = dateOfIssue;
    }
    if (durationDays != null) {
      $result.durationDays = durationDays;
    }
    if (title != null) {
      $result.title = title;
    }
    if (dosageMg != null) {
      $result.dosageMg = dosageMg;
    }
    if (wayToUse != null) {
      $result.wayToUse = wayToUse;
    }
    return $result;
  }
  PrescriptionUpdateRequest._() : super();
  factory PrescriptionUpdateRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PrescriptionUpdateRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(_omitMessageNames ? '' : 'PrescriptionUpdateRequest', package: const $pb.PackageName(_omitMessageNames ? '' : 'mis'), createEmptyInstance: create)
    ..aInt64(1, _omitFieldNames ? '' : 'id')
    ..aInt64(2, _omitFieldNames ? '' : 'patientId')
    ..aInt64(3, _omitFieldNames ? '' : 'doctorId')
    ..aOS(4, _omitFieldNames ? '' : 'dateOfIssue')
    ..aInt64(5, _omitFieldNames ? '' : 'durationDays')
    ..aOS(6, _omitFieldNames ? '' : 'title')
    ..aInt64(7, _omitFieldNames ? '' : 'dosageMg')
    ..aOS(8, _omitFieldNames ? '' : 'wayToUse')
    ..hasRequiredFields = false
  ;

  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PrescriptionUpdateRequest clone() => PrescriptionUpdateRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PrescriptionUpdateRequest copyWith(void Function(PrescriptionUpdateRequest) updates) => super.copyWith((message) => updates(message as PrescriptionUpdateRequest)) as PrescriptionUpdateRequest;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static PrescriptionUpdateRequest create() => PrescriptionUpdateRequest._();
  PrescriptionUpdateRequest createEmptyInstance() => create();
  static $pb.PbList<PrescriptionUpdateRequest> createRepeated() => $pb.PbList<PrescriptionUpdateRequest>();
  @$core.pragma('dart2js:noInline')
  static PrescriptionUpdateRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PrescriptionUpdateRequest>(create);
  static PrescriptionUpdateRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get id => $_getI64(0);
  @$pb.TagNumber(1)
  set id($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get patientId => $_getI64(1);
  @$pb.TagNumber(2)
  set patientId($fixnum.Int64 v) { $_setInt64(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPatientId() => $_has(1);
  @$pb.TagNumber(2)
  void clearPatientId() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get doctorId => $_getI64(2);
  @$pb.TagNumber(3)
  set doctorId($fixnum.Int64 v) { $_setInt64(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasDoctorId() => $_has(2);
  @$pb.TagNumber(3)
  void clearDoctorId() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get dateOfIssue => $_getSZ(3);
  @$pb.TagNumber(4)
  set dateOfIssue($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasDateOfIssue() => $_has(3);
  @$pb.TagNumber(4)
  void clearDateOfIssue() => clearField(4);

  @$pb.TagNumber(5)
  $fixnum.Int64 get durationDays => $_getI64(4);
  @$pb.TagNumber(5)
  set durationDays($fixnum.Int64 v) { $_setInt64(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasDurationDays() => $_has(4);
  @$pb.TagNumber(5)
  void clearDurationDays() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get title => $_getSZ(5);
  @$pb.TagNumber(6)
  set title($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasTitle() => $_has(5);
  @$pb.TagNumber(6)
  void clearTitle() => clearField(6);

  @$pb.TagNumber(7)
  $fixnum.Int64 get dosageMg => $_getI64(6);
  @$pb.TagNumber(7)
  set dosageMg($fixnum.Int64 v) { $_setInt64(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasDosageMg() => $_has(6);
  @$pb.TagNumber(7)
  void clearDosageMg() => clearField(7);

  @$pb.TagNumber(8)
  $core.String get wayToUse => $_getSZ(7);
  @$pb.TagNumber(8)
  set wayToUse($core.String v) { $_setString(7, v); }
  @$pb.TagNumber(8)
  $core.bool hasWayToUse() => $_has(7);
  @$pb.TagNumber(8)
  void clearWayToUse() => clearField(8);
}

class PatientResponse extends $pb.GeneratedMessage {
  factory PatientResponse({
    $fixnum.Int64? id,
    $core.String? email,
    $core.String? password,
    $core.String? firstName,
    $core.String? lastName,
    $core.String? patronymic,
    $core.String? phoneNumber,
  }) {
    final $result = create();
    if (id != null) {
      $result.id = id;
    }
    if (email != null) {
      $result.email = email;
    }
    if (password != null) {
      $result.password = password;
    }
    if (firstName != null) {
      $result.firstName = firstName;
    }
    if (lastName != null) {
      $result.lastName = lastName;
    }
    if (patronymic != null) {
      $result.patronymic = patronymic;
    }
    if (phoneNumber != null) {
      $result.phoneNumber = phoneNumber;
    }
    return $result;
  }
  PatientResponse._() : super();
  factory PatientResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PatientResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(_omitMessageNames ? '' : 'PatientResponse', package: const $pb.PackageName(_omitMessageNames ? '' : 'mis'), createEmptyInstance: create)
    ..aInt64(1, _omitFieldNames ? '' : 'id')
    ..aOS(2, _omitFieldNames ? '' : 'email')
    ..aOS(3, _omitFieldNames ? '' : 'password')
    ..aOS(4, _omitFieldNames ? '' : 'firstName')
    ..aOS(5, _omitFieldNames ? '' : 'lastName')
    ..aOS(6, _omitFieldNames ? '' : 'patronymic')
    ..aOS(7, _omitFieldNames ? '' : 'phoneNumber')
    ..hasRequiredFields = false
  ;

  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PatientResponse clone() => PatientResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PatientResponse copyWith(void Function(PatientResponse) updates) => super.copyWith((message) => updates(message as PatientResponse)) as PatientResponse;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static PatientResponse create() => PatientResponse._();
  PatientResponse createEmptyInstance() => create();
  static $pb.PbList<PatientResponse> createRepeated() => $pb.PbList<PatientResponse>();
  @$core.pragma('dart2js:noInline')
  static PatientResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PatientResponse>(create);
  static PatientResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get id => $_getI64(0);
  @$pb.TagNumber(1)
  set id($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get email => $_getSZ(1);
  @$pb.TagNumber(2)
  set email($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasEmail() => $_has(1);
  @$pb.TagNumber(2)
  void clearEmail() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get password => $_getSZ(2);
  @$pb.TagNumber(3)
  set password($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasPassword() => $_has(2);
  @$pb.TagNumber(3)
  void clearPassword() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get firstName => $_getSZ(3);
  @$pb.TagNumber(4)
  set firstName($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasFirstName() => $_has(3);
  @$pb.TagNumber(4)
  void clearFirstName() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get lastName => $_getSZ(4);
  @$pb.TagNumber(5)
  set lastName($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasLastName() => $_has(4);
  @$pb.TagNumber(5)
  void clearLastName() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get patronymic => $_getSZ(5);
  @$pb.TagNumber(6)
  set patronymic($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasPatronymic() => $_has(5);
  @$pb.TagNumber(6)
  void clearPatronymic() => clearField(6);

  @$pb.TagNumber(7)
  $core.String get phoneNumber => $_getSZ(6);
  @$pb.TagNumber(7)
  set phoneNumber($core.String v) { $_setString(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasPhoneNumber() => $_has(6);
  @$pb.TagNumber(7)
  void clearPhoneNumber() => clearField(7);
}

class PatientList extends $pb.GeneratedMessage {
  factory PatientList({
    $core.Iterable<PatientResponse>? patients,
  }) {
    final $result = create();
    if (patients != null) {
      $result.patients.addAll(patients);
    }
    return $result;
  }
  PatientList._() : super();
  factory PatientList.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PatientList.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(_omitMessageNames ? '' : 'PatientList', package: const $pb.PackageName(_omitMessageNames ? '' : 'mis'), createEmptyInstance: create)
    ..pc<PatientResponse>(1, _omitFieldNames ? '' : 'patients', $pb.PbFieldType.PM, subBuilder: PatientResponse.create)
    ..hasRequiredFields = false
  ;

  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PatientList clone() => PatientList()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PatientList copyWith(void Function(PatientList) updates) => super.copyWith((message) => updates(message as PatientList)) as PatientList;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static PatientList create() => PatientList._();
  PatientList createEmptyInstance() => create();
  static $pb.PbList<PatientList> createRepeated() => $pb.PbList<PatientList>();
  @$core.pragma('dart2js:noInline')
  static PatientList getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PatientList>(create);
  static PatientList? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<PatientResponse> get patients => $_getList(0);
}

class DoctorResponse extends $pb.GeneratedMessage {
  factory DoctorResponse({
    $fixnum.Int64? id,
    $core.String? email,
    $core.String? password,
    $core.String? firstName,
    $core.String? lastName,
    $core.String? patronymic,
    $core.String? phoneNumber,
    $core.String? specialty,
    $core.String? license,
  }) {
    final $result = create();
    if (id != null) {
      $result.id = id;
    }
    if (email != null) {
      $result.email = email;
    }
    if (password != null) {
      $result.password = password;
    }
    if (firstName != null) {
      $result.firstName = firstName;
    }
    if (lastName != null) {
      $result.lastName = lastName;
    }
    if (patronymic != null) {
      $result.patronymic = patronymic;
    }
    if (phoneNumber != null) {
      $result.phoneNumber = phoneNumber;
    }
    if (specialty != null) {
      $result.specialty = specialty;
    }
    if (license != null) {
      $result.license = license;
    }
    return $result;
  }
  DoctorResponse._() : super();
  factory DoctorResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory DoctorResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(_omitMessageNames ? '' : 'DoctorResponse', package: const $pb.PackageName(_omitMessageNames ? '' : 'mis'), createEmptyInstance: create)
    ..aInt64(1, _omitFieldNames ? '' : 'id')
    ..aOS(2, _omitFieldNames ? '' : 'email')
    ..aOS(3, _omitFieldNames ? '' : 'password')
    ..aOS(4, _omitFieldNames ? '' : 'firstName')
    ..aOS(5, _omitFieldNames ? '' : 'lastName')
    ..aOS(6, _omitFieldNames ? '' : 'patronymic')
    ..aOS(7, _omitFieldNames ? '' : 'phoneNumber')
    ..aOS(8, _omitFieldNames ? '' : 'specialty')
    ..aOS(9, _omitFieldNames ? '' : 'license')
    ..hasRequiredFields = false
  ;

  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  DoctorResponse clone() => DoctorResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  DoctorResponse copyWith(void Function(DoctorResponse) updates) => super.copyWith((message) => updates(message as DoctorResponse)) as DoctorResponse;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static DoctorResponse create() => DoctorResponse._();
  DoctorResponse createEmptyInstance() => create();
  static $pb.PbList<DoctorResponse> createRepeated() => $pb.PbList<DoctorResponse>();
  @$core.pragma('dart2js:noInline')
  static DoctorResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<DoctorResponse>(create);
  static DoctorResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get id => $_getI64(0);
  @$pb.TagNumber(1)
  set id($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get email => $_getSZ(1);
  @$pb.TagNumber(2)
  set email($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasEmail() => $_has(1);
  @$pb.TagNumber(2)
  void clearEmail() => clearField(2);

  @$pb.TagNumber(3)
  $core.String get password => $_getSZ(2);
  @$pb.TagNumber(3)
  set password($core.String v) { $_setString(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasPassword() => $_has(2);
  @$pb.TagNumber(3)
  void clearPassword() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get firstName => $_getSZ(3);
  @$pb.TagNumber(4)
  set firstName($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasFirstName() => $_has(3);
  @$pb.TagNumber(4)
  void clearFirstName() => clearField(4);

  @$pb.TagNumber(5)
  $core.String get lastName => $_getSZ(4);
  @$pb.TagNumber(5)
  set lastName($core.String v) { $_setString(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasLastName() => $_has(4);
  @$pb.TagNumber(5)
  void clearLastName() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get patronymic => $_getSZ(5);
  @$pb.TagNumber(6)
  set patronymic($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasPatronymic() => $_has(5);
  @$pb.TagNumber(6)
  void clearPatronymic() => clearField(6);

  @$pb.TagNumber(7)
  $core.String get phoneNumber => $_getSZ(6);
  @$pb.TagNumber(7)
  set phoneNumber($core.String v) { $_setString(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasPhoneNumber() => $_has(6);
  @$pb.TagNumber(7)
  void clearPhoneNumber() => clearField(7);

  @$pb.TagNumber(8)
  $core.String get specialty => $_getSZ(7);
  @$pb.TagNumber(8)
  set specialty($core.String v) { $_setString(7, v); }
  @$pb.TagNumber(8)
  $core.bool hasSpecialty() => $_has(7);
  @$pb.TagNumber(8)
  void clearSpecialty() => clearField(8);

  @$pb.TagNumber(9)
  $core.String get license => $_getSZ(8);
  @$pb.TagNumber(9)
  set license($core.String v) { $_setString(8, v); }
  @$pb.TagNumber(9)
  $core.bool hasLicense() => $_has(8);
  @$pb.TagNumber(9)
  void clearLicense() => clearField(9);
}

class DoctorList extends $pb.GeneratedMessage {
  factory DoctorList({
    $core.Iterable<DoctorResponse>? doctors,
  }) {
    final $result = create();
    if (doctors != null) {
      $result.doctors.addAll(doctors);
    }
    return $result;
  }
  DoctorList._() : super();
  factory DoctorList.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory DoctorList.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(_omitMessageNames ? '' : 'DoctorList', package: const $pb.PackageName(_omitMessageNames ? '' : 'mis'), createEmptyInstance: create)
    ..pc<DoctorResponse>(1, _omitFieldNames ? '' : 'doctors', $pb.PbFieldType.PM, subBuilder: DoctorResponse.create)
    ..hasRequiredFields = false
  ;

  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  DoctorList clone() => DoctorList()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  DoctorList copyWith(void Function(DoctorList) updates) => super.copyWith((message) => updates(message as DoctorList)) as DoctorList;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static DoctorList create() => DoctorList._();
  DoctorList createEmptyInstance() => create();
  static $pb.PbList<DoctorList> createRepeated() => $pb.PbList<DoctorList>();
  @$core.pragma('dart2js:noInline')
  static DoctorList getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<DoctorList>(create);
  static DoctorList? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<DoctorResponse> get doctors => $_getList(0);
}

class PrescriptionResponse extends $pb.GeneratedMessage {
  factory PrescriptionResponse({
    $fixnum.Int64? id,
    $fixnum.Int64? patientId,
    $fixnum.Int64? doctorId,
    $core.String? dateOfIssue,
    $fixnum.Int64? durationDays,
    $core.String? title,
    $fixnum.Int64? dosageMg,
    $core.String? wayToUse,
  }) {
    final $result = create();
    if (id != null) {
      $result.id = id;
    }
    if (patientId != null) {
      $result.patientId = patientId;
    }
    if (doctorId != null) {
      $result.doctorId = doctorId;
    }
    if (dateOfIssue != null) {
      $result.dateOfIssue = dateOfIssue;
    }
    if (durationDays != null) {
      $result.durationDays = durationDays;
    }
    if (title != null) {
      $result.title = title;
    }
    if (dosageMg != null) {
      $result.dosageMg = dosageMg;
    }
    if (wayToUse != null) {
      $result.wayToUse = wayToUse;
    }
    return $result;
  }
  PrescriptionResponse._() : super();
  factory PrescriptionResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PrescriptionResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(_omitMessageNames ? '' : 'PrescriptionResponse', package: const $pb.PackageName(_omitMessageNames ? '' : 'mis'), createEmptyInstance: create)
    ..aInt64(1, _omitFieldNames ? '' : 'id')
    ..aInt64(2, _omitFieldNames ? '' : 'patientId')
    ..aInt64(3, _omitFieldNames ? '' : 'doctorId')
    ..aOS(4, _omitFieldNames ? '' : 'dateOfIssue')
    ..aInt64(5, _omitFieldNames ? '' : 'durationDays')
    ..aOS(6, _omitFieldNames ? '' : 'title')
    ..aInt64(7, _omitFieldNames ? '' : 'dosageMg')
    ..aOS(8, _omitFieldNames ? '' : 'wayToUse')
    ..hasRequiredFields = false
  ;

  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PrescriptionResponse clone() => PrescriptionResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PrescriptionResponse copyWith(void Function(PrescriptionResponse) updates) => super.copyWith((message) => updates(message as PrescriptionResponse)) as PrescriptionResponse;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static PrescriptionResponse create() => PrescriptionResponse._();
  PrescriptionResponse createEmptyInstance() => create();
  static $pb.PbList<PrescriptionResponse> createRepeated() => $pb.PbList<PrescriptionResponse>();
  @$core.pragma('dart2js:noInline')
  static PrescriptionResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PrescriptionResponse>(create);
  static PrescriptionResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get id => $_getI64(0);
  @$pb.TagNumber(1)
  set id($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get patientId => $_getI64(1);
  @$pb.TagNumber(2)
  set patientId($fixnum.Int64 v) { $_setInt64(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPatientId() => $_has(1);
  @$pb.TagNumber(2)
  void clearPatientId() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get doctorId => $_getI64(2);
  @$pb.TagNumber(3)
  set doctorId($fixnum.Int64 v) { $_setInt64(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasDoctorId() => $_has(2);
  @$pb.TagNumber(3)
  void clearDoctorId() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get dateOfIssue => $_getSZ(3);
  @$pb.TagNumber(4)
  set dateOfIssue($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasDateOfIssue() => $_has(3);
  @$pb.TagNumber(4)
  void clearDateOfIssue() => clearField(4);

  @$pb.TagNumber(5)
  $fixnum.Int64 get durationDays => $_getI64(4);
  @$pb.TagNumber(5)
  set durationDays($fixnum.Int64 v) { $_setInt64(4, v); }
  @$pb.TagNumber(5)
  $core.bool hasDurationDays() => $_has(4);
  @$pb.TagNumber(5)
  void clearDurationDays() => clearField(5);

  @$pb.TagNumber(6)
  $core.String get title => $_getSZ(5);
  @$pb.TagNumber(6)
  set title($core.String v) { $_setString(5, v); }
  @$pb.TagNumber(6)
  $core.bool hasTitle() => $_has(5);
  @$pb.TagNumber(6)
  void clearTitle() => clearField(6);

  @$pb.TagNumber(7)
  $fixnum.Int64 get dosageMg => $_getI64(6);
  @$pb.TagNumber(7)
  set dosageMg($fixnum.Int64 v) { $_setInt64(6, v); }
  @$pb.TagNumber(7)
  $core.bool hasDosageMg() => $_has(6);
  @$pb.TagNumber(7)
  void clearDosageMg() => clearField(7);

  @$pb.TagNumber(8)
  $core.String get wayToUse => $_getSZ(7);
  @$pb.TagNumber(8)
  set wayToUse($core.String v) { $_setString(7, v); }
  @$pb.TagNumber(8)
  $core.bool hasWayToUse() => $_has(7);
  @$pb.TagNumber(8)
  void clearWayToUse() => clearField(8);
}

class PrescriptionList extends $pb.GeneratedMessage {
  factory PrescriptionList({
    $core.Iterable<PrescriptionResponse>? prescriptions,
  }) {
    final $result = create();
    if (prescriptions != null) {
      $result.prescriptions.addAll(prescriptions);
    }
    return $result;
  }
  PrescriptionList._() : super();
  factory PrescriptionList.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PrescriptionList.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(_omitMessageNames ? '' : 'PrescriptionList', package: const $pb.PackageName(_omitMessageNames ? '' : 'mis'), createEmptyInstance: create)
    ..pc<PrescriptionResponse>(1, _omitFieldNames ? '' : 'prescriptions', $pb.PbFieldType.PM, subBuilder: PrescriptionResponse.create)
    ..hasRequiredFields = false
  ;

  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PrescriptionList clone() => PrescriptionList()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PrescriptionList copyWith(void Function(PrescriptionList) updates) => super.copyWith((message) => updates(message as PrescriptionList)) as PrescriptionList;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static PrescriptionList create() => PrescriptionList._();
  PrescriptionList createEmptyInstance() => create();
  static $pb.PbList<PrescriptionList> createRepeated() => $pb.PbList<PrescriptionList>();
  @$core.pragma('dart2js:noInline')
  static PrescriptionList getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PrescriptionList>(create);
  static PrescriptionList? _defaultInstance;

  @$pb.TagNumber(1)
  $core.List<PrescriptionResponse> get prescriptions => $_getList(0);
}

class LoginRequest extends $pb.GeneratedMessage {
  factory LoginRequest({
    $core.String? email,
    $core.String? password,
  }) {
    final $result = create();
    if (email != null) {
      $result.email = email;
    }
    if (password != null) {
      $result.password = password;
    }
    return $result;
  }
  LoginRequest._() : super();
  factory LoginRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory LoginRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(_omitMessageNames ? '' : 'LoginRequest', package: const $pb.PackageName(_omitMessageNames ? '' : 'mis'), createEmptyInstance: create)
    ..aOS(1, _omitFieldNames ? '' : 'email')
    ..aOS(2, _omitFieldNames ? '' : 'password')
    ..hasRequiredFields = false
  ;

  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  LoginRequest clone() => LoginRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  LoginRequest copyWith(void Function(LoginRequest) updates) => super.copyWith((message) => updates(message as LoginRequest)) as LoginRequest;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static LoginRequest create() => LoginRequest._();
  LoginRequest createEmptyInstance() => create();
  static $pb.PbList<LoginRequest> createRepeated() => $pb.PbList<LoginRequest>();
  @$core.pragma('dart2js:noInline')
  static LoginRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<LoginRequest>(create);
  static LoginRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get email => $_getSZ(0);
  @$pb.TagNumber(1)
  set email($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasEmail() => $_has(0);
  @$pb.TagNumber(1)
  void clearEmail() => clearField(1);

  @$pb.TagNumber(2)
  $core.String get password => $_getSZ(1);
  @$pb.TagNumber(2)
  set password($core.String v) { $_setString(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasPassword() => $_has(1);
  @$pb.TagNumber(2)
  void clearPassword() => clearField(2);
}

class LoginResponse extends $pb.GeneratedMessage {
  factory LoginResponse({
    $core.String? token,
    $fixnum.Int64? id,
  }) {
    final $result = create();
    if (token != null) {
      $result.token = token;
    }
    if (id != null) {
      $result.id = id;
    }
    return $result;
  }
  LoginResponse._() : super();
  factory LoginResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory LoginResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(_omitMessageNames ? '' : 'LoginResponse', package: const $pb.PackageName(_omitMessageNames ? '' : 'mis'), createEmptyInstance: create)
    ..aOS(1, _omitFieldNames ? '' : 'token')
    ..aInt64(2, _omitFieldNames ? '' : 'id')
    ..hasRequiredFields = false
  ;

  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  LoginResponse clone() => LoginResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  LoginResponse copyWith(void Function(LoginResponse) updates) => super.copyWith((message) => updates(message as LoginResponse)) as LoginResponse;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static LoginResponse create() => LoginResponse._();
  LoginResponse createEmptyInstance() => create();
  static $pb.PbList<LoginResponse> createRepeated() => $pb.PbList<LoginResponse>();
  @$core.pragma('dart2js:noInline')
  static LoginResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<LoginResponse>(create);
  static LoginResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get token => $_getSZ(0);
  @$pb.TagNumber(1)
  set token($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasToken() => $_has(0);
  @$pb.TagNumber(1)
  void clearToken() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get id => $_getI64(1);
  @$pb.TagNumber(2)
  set id($fixnum.Int64 v) { $_setInt64(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasId() => $_has(1);
  @$pb.TagNumber(2)
  void clearId() => clearField(2);
}

class EmailRequest extends $pb.GeneratedMessage {
  factory EmailRequest({
    $core.String? email,
  }) {
    final $result = create();
    if (email != null) {
      $result.email = email;
    }
    return $result;
  }
  EmailRequest._() : super();
  factory EmailRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory EmailRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(_omitMessageNames ? '' : 'EmailRequest', package: const $pb.PackageName(_omitMessageNames ? '' : 'mis'), createEmptyInstance: create)
    ..aOS(1, _omitFieldNames ? '' : 'email')
    ..hasRequiredFields = false
  ;

  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  EmailRequest clone() => EmailRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  EmailRequest copyWith(void Function(EmailRequest) updates) => super.copyWith((message) => updates(message as EmailRequest)) as EmailRequest;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static EmailRequest create() => EmailRequest._();
  EmailRequest createEmptyInstance() => create();
  static $pb.PbList<EmailRequest> createRepeated() => $pb.PbList<EmailRequest>();
  @$core.pragma('dart2js:noInline')
  static EmailRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<EmailRequest>(create);
  static EmailRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $core.String get email => $_getSZ(0);
  @$pb.TagNumber(1)
  set email($core.String v) { $_setString(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasEmail() => $_has(0);
  @$pb.TagNumber(1)
  void clearEmail() => clearField(1);
}

class PatientDoctorIDRequest extends $pb.GeneratedMessage {
  factory PatientDoctorIDRequest({
    $fixnum.Int64? patientId,
    $fixnum.Int64? doctorId,
  }) {
    final $result = create();
    if (patientId != null) {
      $result.patientId = patientId;
    }
    if (doctorId != null) {
      $result.doctorId = doctorId;
    }
    return $result;
  }
  PatientDoctorIDRequest._() : super();
  factory PatientDoctorIDRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory PatientDoctorIDRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(_omitMessageNames ? '' : 'PatientDoctorIDRequest', package: const $pb.PackageName(_omitMessageNames ? '' : 'mis'), createEmptyInstance: create)
    ..aInt64(1, _omitFieldNames ? '' : 'patientId')
    ..aInt64(2, _omitFieldNames ? '' : 'doctorId')
    ..hasRequiredFields = false
  ;

  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  PatientDoctorIDRequest clone() => PatientDoctorIDRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  PatientDoctorIDRequest copyWith(void Function(PatientDoctorIDRequest) updates) => super.copyWith((message) => updates(message as PatientDoctorIDRequest)) as PatientDoctorIDRequest;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static PatientDoctorIDRequest create() => PatientDoctorIDRequest._();
  PatientDoctorIDRequest createEmptyInstance() => create();
  static $pb.PbList<PatientDoctorIDRequest> createRepeated() => $pb.PbList<PatientDoctorIDRequest>();
  @$core.pragma('dart2js:noInline')
  static PatientDoctorIDRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<PatientDoctorIDRequest>(create);
  static PatientDoctorIDRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get patientId => $_getI64(0);
  @$pb.TagNumber(1)
  set patientId($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasPatientId() => $_has(0);
  @$pb.TagNumber(1)
  void clearPatientId() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get doctorId => $_getI64(1);
  @$pb.TagNumber(2)
  set doctorId($fixnum.Int64 v) { $_setInt64(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasDoctorId() => $_has(1);
  @$pb.TagNumber(2)
  void clearDoctorId() => clearField(2);
}

class Message extends $pb.GeneratedMessage {
  factory Message({
    $fixnum.Int64? id,
    $fixnum.Int64? receiverId,
    $fixnum.Int64? senderId,
    $core.String? message,
    $0.Timestamp? createdAt,
  }) {
    final $result = create();
    if (id != null) {
      $result.id = id;
    }
    if (receiverId != null) {
      $result.receiverId = receiverId;
    }
    if (senderId != null) {
      $result.senderId = senderId;
    }
    if (message != null) {
      $result.message = message;
    }
    if (createdAt != null) {
      $result.createdAt = createdAt;
    }
    return $result;
  }
  Message._() : super();
  factory Message.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Message.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(_omitMessageNames ? '' : 'Message', package: const $pb.PackageName(_omitMessageNames ? '' : 'mis'), createEmptyInstance: create)
    ..aInt64(1, _omitFieldNames ? '' : 'id')
    ..aInt64(2, _omitFieldNames ? '' : 'receiverId')
    ..aInt64(3, _omitFieldNames ? '' : 'senderId')
    ..aOS(4, _omitFieldNames ? '' : 'message')
    ..aOM<$0.Timestamp>(5, _omitFieldNames ? '' : 'createdAt', subBuilder: $0.Timestamp.create)
    ..hasRequiredFields = false
  ;

  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Message clone() => Message()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Message copyWith(void Function(Message) updates) => super.copyWith((message) => updates(message as Message)) as Message;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static Message create() => Message._();
  Message createEmptyInstance() => create();
  static $pb.PbList<Message> createRepeated() => $pb.PbList<Message>();
  @$core.pragma('dart2js:noInline')
  static Message getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Message>(create);
  static Message? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get id => $_getI64(0);
  @$pb.TagNumber(1)
  set id($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get receiverId => $_getI64(1);
  @$pb.TagNumber(2)
  set receiverId($fixnum.Int64 v) { $_setInt64(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasReceiverId() => $_has(1);
  @$pb.TagNumber(2)
  void clearReceiverId() => clearField(2);

  @$pb.TagNumber(3)
  $fixnum.Int64 get senderId => $_getI64(2);
  @$pb.TagNumber(3)
  set senderId($fixnum.Int64 v) { $_setInt64(2, v); }
  @$pb.TagNumber(3)
  $core.bool hasSenderId() => $_has(2);
  @$pb.TagNumber(3)
  void clearSenderId() => clearField(3);

  @$pb.TagNumber(4)
  $core.String get message => $_getSZ(3);
  @$pb.TagNumber(4)
  set message($core.String v) { $_setString(3, v); }
  @$pb.TagNumber(4)
  $core.bool hasMessage() => $_has(3);
  @$pb.TagNumber(4)
  void clearMessage() => clearField(4);

  @$pb.TagNumber(5)
  $0.Timestamp get createdAt => $_getN(4);
  @$pb.TagNumber(5)
  set createdAt($0.Timestamp v) { setField(5, v); }
  @$pb.TagNumber(5)
  $core.bool hasCreatedAt() => $_has(4);
  @$pb.TagNumber(5)
  void clearCreatedAt() => clearField(5);
  @$pb.TagNumber(5)
  $0.Timestamp ensureCreatedAt() => $_ensure(4);
}

class JoinSessionRequest extends $pb.GeneratedMessage {
  factory JoinSessionRequest({
    $fixnum.Int64? senderId,
    $fixnum.Int64? receiverId,
  }) {
    final $result = create();
    if (senderId != null) {
      $result.senderId = senderId;
    }
    if (receiverId != null) {
      $result.receiverId = receiverId;
    }
    return $result;
  }
  JoinSessionRequest._() : super();
  factory JoinSessionRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory JoinSessionRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(_omitMessageNames ? '' : 'JoinSessionRequest', package: const $pb.PackageName(_omitMessageNames ? '' : 'mis'), createEmptyInstance: create)
    ..aInt64(1, _omitFieldNames ? '' : 'senderId')
    ..aInt64(2, _omitFieldNames ? '' : 'receiverId')
    ..hasRequiredFields = false
  ;

  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  JoinSessionRequest clone() => JoinSessionRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  JoinSessionRequest copyWith(void Function(JoinSessionRequest) updates) => super.copyWith((message) => updates(message as JoinSessionRequest)) as JoinSessionRequest;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static JoinSessionRequest create() => JoinSessionRequest._();
  JoinSessionRequest createEmptyInstance() => create();
  static $pb.PbList<JoinSessionRequest> createRepeated() => $pb.PbList<JoinSessionRequest>();
  @$core.pragma('dart2js:noInline')
  static JoinSessionRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<JoinSessionRequest>(create);
  static JoinSessionRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get senderId => $_getI64(0);
  @$pb.TagNumber(1)
  set senderId($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasSenderId() => $_has(0);
  @$pb.TagNumber(1)
  void clearSenderId() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get receiverId => $_getI64(1);
  @$pb.TagNumber(2)
  set receiverId($fixnum.Int64 v) { $_setInt64(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasReceiverId() => $_has(1);
  @$pb.TagNumber(2)
  void clearReceiverId() => clearField(2);
}

/// General messages
class IDRequest extends $pb.GeneratedMessage {
  factory IDRequest({
    $fixnum.Int64? id,
  }) {
    final $result = create();
    if (id != null) {
      $result.id = id;
    }
    return $result;
  }
  IDRequest._() : super();
  factory IDRequest.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory IDRequest.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(_omitMessageNames ? '' : 'IDRequest', package: const $pb.PackageName(_omitMessageNames ? '' : 'mis'), createEmptyInstance: create)
    ..aInt64(1, _omitFieldNames ? '' : 'id')
    ..hasRequiredFields = false
  ;

  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  IDRequest clone() => IDRequest()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  IDRequest copyWith(void Function(IDRequest) updates) => super.copyWith((message) => updates(message as IDRequest)) as IDRequest;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static IDRequest create() => IDRequest._();
  IDRequest createEmptyInstance() => create();
  static $pb.PbList<IDRequest> createRepeated() => $pb.PbList<IDRequest>();
  @$core.pragma('dart2js:noInline')
  static IDRequest getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<IDRequest>(create);
  static IDRequest? _defaultInstance;

  @$pb.TagNumber(1)
  $fixnum.Int64 get id => $_getI64(0);
  @$pb.TagNumber(1)
  set id($fixnum.Int64 v) { $_setInt64(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasId() => $_has(0);
  @$pb.TagNumber(1)
  void clearId() => clearField(1);
}

class CreateResponse extends $pb.GeneratedMessage {
  factory CreateResponse({
    $core.bool? success,
    $fixnum.Int64? id,
  }) {
    final $result = create();
    if (success != null) {
      $result.success = success;
    }
    if (id != null) {
      $result.id = id;
    }
    return $result;
  }
  CreateResponse._() : super();
  factory CreateResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory CreateResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(_omitMessageNames ? '' : 'CreateResponse', package: const $pb.PackageName(_omitMessageNames ? '' : 'mis'), createEmptyInstance: create)
    ..aOB(1, _omitFieldNames ? '' : 'success')
    ..aInt64(2, _omitFieldNames ? '' : 'id')
    ..hasRequiredFields = false
  ;

  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  CreateResponse clone() => CreateResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  CreateResponse copyWith(void Function(CreateResponse) updates) => super.copyWith((message) => updates(message as CreateResponse)) as CreateResponse;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static CreateResponse create() => CreateResponse._();
  CreateResponse createEmptyInstance() => create();
  static $pb.PbList<CreateResponse> createRepeated() => $pb.PbList<CreateResponse>();
  @$core.pragma('dart2js:noInline')
  static CreateResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<CreateResponse>(create);
  static CreateResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.bool get success => $_getBF(0);
  @$pb.TagNumber(1)
  set success($core.bool v) { $_setBool(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasSuccess() => $_has(0);
  @$pb.TagNumber(1)
  void clearSuccess() => clearField(1);

  @$pb.TagNumber(2)
  $fixnum.Int64 get id => $_getI64(1);
  @$pb.TagNumber(2)
  set id($fixnum.Int64 v) { $_setInt64(1, v); }
  @$pb.TagNumber(2)
  $core.bool hasId() => $_has(1);
  @$pb.TagNumber(2)
  void clearId() => clearField(2);
}

class SuccessResponse extends $pb.GeneratedMessage {
  factory SuccessResponse({
    $core.bool? success,
  }) {
    final $result = create();
    if (success != null) {
      $result.success = success;
    }
    return $result;
  }
  SuccessResponse._() : super();
  factory SuccessResponse.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory SuccessResponse.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(_omitMessageNames ? '' : 'SuccessResponse', package: const $pb.PackageName(_omitMessageNames ? '' : 'mis'), createEmptyInstance: create)
    ..aOB(1, _omitFieldNames ? '' : 'success')
    ..hasRequiredFields = false
  ;

  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  SuccessResponse clone() => SuccessResponse()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  SuccessResponse copyWith(void Function(SuccessResponse) updates) => super.copyWith((message) => updates(message as SuccessResponse)) as SuccessResponse;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static SuccessResponse create() => SuccessResponse._();
  SuccessResponse createEmptyInstance() => create();
  static $pb.PbList<SuccessResponse> createRepeated() => $pb.PbList<SuccessResponse>();
  @$core.pragma('dart2js:noInline')
  static SuccessResponse getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<SuccessResponse>(create);
  static SuccessResponse? _defaultInstance;

  @$pb.TagNumber(1)
  $core.bool get success => $_getBF(0);
  @$pb.TagNumber(1)
  set success($core.bool v) { $_setBool(0, v); }
  @$pb.TagNumber(1)
  $core.bool hasSuccess() => $_has(0);
  @$pb.TagNumber(1)
  void clearSuccess() => clearField(1);
}

class Empty extends $pb.GeneratedMessage {
  factory Empty() => create();
  Empty._() : super();
  factory Empty.fromBuffer($core.List<$core.int> i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromBuffer(i, r);
  factory Empty.fromJson($core.String i, [$pb.ExtensionRegistry r = $pb.ExtensionRegistry.EMPTY]) => create()..mergeFromJson(i, r);

  static final $pb.BuilderInfo _i = $pb.BuilderInfo(_omitMessageNames ? '' : 'Empty', package: const $pb.PackageName(_omitMessageNames ? '' : 'mis'), createEmptyInstance: create)
    ..hasRequiredFields = false
  ;

  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.deepCopy] instead. '
  'Will be removed in next major version')
  Empty clone() => Empty()..mergeFromMessage(this);
  @$core.Deprecated(
  'Using this can add significant overhead to your binary. '
  'Use [GeneratedMessageGenericExtensions.rebuild] instead. '
  'Will be removed in next major version')
  Empty copyWith(void Function(Empty) updates) => super.copyWith((message) => updates(message as Empty)) as Empty;

  $pb.BuilderInfo get info_ => _i;

  @$core.pragma('dart2js:noInline')
  static Empty create() => Empty._();
  Empty createEmptyInstance() => create();
  static $pb.PbList<Empty> createRepeated() => $pb.PbList<Empty>();
  @$core.pragma('dart2js:noInline')
  static Empty getDefault() => _defaultInstance ??= $pb.GeneratedMessage.$_defaultFor<Empty>(create);
  static Empty? _defaultInstance;
}

class MISApi {
  $pb.RpcClient _client;
  MISApi(this._client);

  $async.Future<CreateResponse> registerPatient($pb.ClientContext? ctx, PatientRequest request) =>
    _client.invoke<CreateResponse>(ctx, 'MIS', 'RegisterPatient', request, CreateResponse())
  ;
  $async.Future<CreateResponse> registerDoctor($pb.ClientContext? ctx, DoctorRequest request) =>
    _client.invoke<CreateResponse>(ctx, 'MIS', 'RegisterDoctor', request, CreateResponse())
  ;
  $async.Future<CreateResponse> registerAdmin($pb.ClientContext? ctx, AdminRequest request) =>
    _client.invoke<CreateResponse>(ctx, 'MIS', 'RegisterAdmin', request, CreateResponse())
  ;
  $async.Future<LoginResponse> login($pb.ClientContext? ctx, LoginRequest request) =>
    _client.invoke<LoginResponse>(ctx, 'MIS', 'Login', request, LoginResponse())
  ;
  $async.Future<PatientResponse> getPatientByEmail($pb.ClientContext? ctx, EmailRequest request) =>
    _client.invoke<PatientResponse>(ctx, 'MIS', 'GetPatientByEmail', request, PatientResponse())
  ;
  $async.Future<PatientResponse> getPatientByID($pb.ClientContext? ctx, IDRequest request) =>
    _client.invoke<PatientResponse>(ctx, 'MIS', 'GetPatientByID', request, PatientResponse())
  ;
  $async.Future<PrescriptionList> getPatientPrescriptions($pb.ClientContext? ctx, IDRequest request) =>
    _client.invoke<PrescriptionList>(ctx, 'MIS', 'GetPatientPrescriptions', request, PrescriptionList())
  ;
  $async.Future<DoctorList> getPatientDoctors($pb.ClientContext? ctx, IDRequest request) =>
    _client.invoke<DoctorList>(ctx, 'MIS', 'GetPatientDoctors', request, DoctorList())
  ;
  $async.Future<SuccessResponse> deletePatient($pb.ClientContext? ctx, IDRequest request) =>
    _client.invoke<SuccessResponse>(ctx, 'MIS', 'DeletePatient', request, SuccessResponse())
  ;
  $async.Future<DoctorResponse> getDoctorByEmail($pb.ClientContext? ctx, EmailRequest request) =>
    _client.invoke<DoctorResponse>(ctx, 'MIS', 'GetDoctorByEmail', request, DoctorResponse())
  ;
  $async.Future<DoctorResponse> getDoctorByID($pb.ClientContext? ctx, IDRequest request) =>
    _client.invoke<DoctorResponse>(ctx, 'MIS', 'GetDoctorByID', request, DoctorResponse())
  ;
  $async.Future<SuccessResponse> addPatient($pb.ClientContext? ctx, EmailRequest request) =>
    _client.invoke<SuccessResponse>(ctx, 'MIS', 'AddPatient', request, SuccessResponse())
  ;
  $async.Future<PrescriptionList> getDoctorPrescriptions($pb.ClientContext? ctx, IDRequest request) =>
    _client.invoke<PrescriptionList>(ctx, 'MIS', 'GetDoctorPrescriptions', request, PrescriptionList())
  ;
  $async.Future<PatientList> getDoctorPatients($pb.ClientContext? ctx, IDRequest request) =>
    _client.invoke<PatientList>(ctx, 'MIS', 'GetDoctorPatients', request, PatientList())
  ;
  $async.Future<PatientList> getDoctorPatientsWithPrescriptions($pb.ClientContext? ctx, IDRequest request) =>
    _client.invoke<PatientList>(ctx, 'MIS', 'GetDoctorPatientsWithPrescriptions', request, PatientList())
  ;
  $async.Future<SuccessResponse> deleteDoctor($pb.ClientContext? ctx, IDRequest request) =>
    _client.invoke<SuccessResponse>(ctx, 'MIS', 'DeleteDoctor', request, SuccessResponse())
  ;
  $async.Future<CreateResponse> addPrescription($pb.ClientContext? ctx, PrescriptionRequest request) =>
    _client.invoke<CreateResponse>(ctx, 'MIS', 'AddPrescription', request, CreateResponse())
  ;
  $async.Future<PrescriptionResponse> getPrescriptionByID($pb.ClientContext? ctx, IDRequest request) =>
    _client.invoke<PrescriptionResponse>(ctx, 'MIS', 'GetPrescriptionByID', request, PrescriptionResponse())
  ;
  $async.Future<PrescriptionList> getPrescriptionsByBoth($pb.ClientContext? ctx, PatientDoctorIDRequest request) =>
    _client.invoke<PrescriptionList>(ctx, 'MIS', 'GetPrescriptionsByBoth', request, PrescriptionList())
  ;
  $async.Future<SuccessResponse> deletePrescription($pb.ClientContext? ctx, IDRequest request) =>
    _client.invoke<SuccessResponse>(ctx, 'MIS', 'DeletePrescription', request, SuccessResponse())
  ;
  $async.Future<SuccessResponse> updatePrescription($pb.ClientContext? ctx, PrescriptionUpdateRequest request) =>
    _client.invoke<SuccessResponse>(ctx, 'MIS', 'UpdatePrescription', request, SuccessResponse())
  ;
}

class ChatServiceApi {
  $pb.RpcClient _client;
  ChatServiceApi(this._client);

  $async.Future<SuccessResponse> sendMessage($pb.ClientContext? ctx, Message request) =>
    _client.invoke<SuccessResponse>(ctx, 'ChatService', 'SendMessage', request, SuccessResponse())
  ;
  $async.Future<Message> receiveMessages($pb.ClientContext? ctx, JoinSessionRequest request) =>
    _client.invoke<Message>(ctx, 'ChatService', 'ReceiveMessages', request, Message())
  ;
  $async.Future<Message> joinChannel($pb.ClientContext? ctx, JoinSessionRequest request) =>
    _client.invoke<Message>(ctx, 'ChatService', 'JoinChannel', request, Message())
  ;
  $async.Future<SuccessResponse> sendMsg($pb.ClientContext? ctx, Message request) =>
    _client.invoke<SuccessResponse>(ctx, 'ChatService', 'SendMsg', request, SuccessResponse())
  ;
}


const _omitFieldNames = $core.bool.fromEnvironment('protobuf.omit_field_names');
const _omitMessageNames = $core.bool.fromEnvironment('protobuf.omit_message_names');
