//
//  Generated code. Do not modify.
//  source: mis/mis.proto
//
// @dart = 2.12

// ignore_for_file: annotate_overrides, camel_case_types, comment_references
// ignore_for_file: constant_identifier_names, library_prefixes
// ignore_for_file: non_constant_identifier_names, prefer_final_fields
// ignore_for_file: unnecessary_import, unnecessary_this, unused_import

import 'dart:convert' as $convert;
import 'dart:core' as $core;
import 'dart:typed_data' as $typed_data;

import '../google/protobuf/timestamp.pbjson.dart' as $0;

@$core.Deprecated('Use patientRequestDescriptor instead')
const PatientRequest$json = {
  '1': 'PatientRequest',
  '2': [
    {'1': 'email', '3': 1, '4': 1, '5': 9, '10': 'email'},
    {'1': 'password', '3': 2, '4': 1, '5': 9, '10': 'password'},
    {'1': 'first_name', '3': 3, '4': 1, '5': 9, '10': 'firstName'},
    {'1': 'last_name', '3': 4, '4': 1, '5': 9, '10': 'lastName'},
    {'1': 'patronymic', '3': 5, '4': 1, '5': 9, '10': 'patronymic'},
    {'1': 'phone_number', '3': 6, '4': 1, '5': 9, '10': 'phoneNumber'},
  ],
};

/// Descriptor for `PatientRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List patientRequestDescriptor = $convert.base64Decode(
    'Cg5QYXRpZW50UmVxdWVzdBIUCgVlbWFpbBgBIAEoCVIFZW1haWwSGgoIcGFzc3dvcmQYAiABKA'
    'lSCHBhc3N3b3JkEh0KCmZpcnN0X25hbWUYAyABKAlSCWZpcnN0TmFtZRIbCglsYXN0X25hbWUY'
    'BCABKAlSCGxhc3ROYW1lEh4KCnBhdHJvbnltaWMYBSABKAlSCnBhdHJvbnltaWMSIQoMcGhvbm'
    'VfbnVtYmVyGAYgASgJUgtwaG9uZU51bWJlcg==');

@$core.Deprecated('Use doctorRequestDescriptor instead')
const DoctorRequest$json = {
  '1': 'DoctorRequest',
  '2': [
    {'1': 'email', '3': 1, '4': 1, '5': 9, '10': 'email'},
    {'1': 'password', '3': 2, '4': 1, '5': 9, '10': 'password'},
    {'1': 'first_name', '3': 3, '4': 1, '5': 9, '10': 'firstName'},
    {'1': 'last_name', '3': 4, '4': 1, '5': 9, '10': 'lastName'},
    {'1': 'patronymic', '3': 5, '4': 1, '5': 9, '10': 'patronymic'},
    {'1': 'phone_number', '3': 6, '4': 1, '5': 9, '10': 'phoneNumber'},
    {'1': 'specialty', '3': 7, '4': 1, '5': 9, '10': 'specialty'},
    {'1': 'license', '3': 8, '4': 1, '5': 9, '10': 'license'},
  ],
};

/// Descriptor for `DoctorRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List doctorRequestDescriptor = $convert.base64Decode(
    'Cg1Eb2N0b3JSZXF1ZXN0EhQKBWVtYWlsGAEgASgJUgVlbWFpbBIaCghwYXNzd29yZBgCIAEoCV'
    'IIcGFzc3dvcmQSHQoKZmlyc3RfbmFtZRgDIAEoCVIJZmlyc3ROYW1lEhsKCWxhc3RfbmFtZRgE'
    'IAEoCVIIbGFzdE5hbWUSHgoKcGF0cm9ueW1pYxgFIAEoCVIKcGF0cm9ueW1pYxIhCgxwaG9uZV'
    '9udW1iZXIYBiABKAlSC3Bob25lTnVtYmVyEhwKCXNwZWNpYWx0eRgHIAEoCVIJc3BlY2lhbHR5'
    'EhgKB2xpY2Vuc2UYCCABKAlSB2xpY2Vuc2U=');

@$core.Deprecated('Use adminRequestDescriptor instead')
const AdminRequest$json = {
  '1': 'AdminRequest',
  '2': [
    {'1': 'email', '3': 1, '4': 1, '5': 9, '10': 'email'},
    {'1': 'password', '3': 2, '4': 1, '5': 9, '10': 'password'},
  ],
};

/// Descriptor for `AdminRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List adminRequestDescriptor = $convert.base64Decode(
    'CgxBZG1pblJlcXVlc3QSFAoFZW1haWwYASABKAlSBWVtYWlsEhoKCHBhc3N3b3JkGAIgASgJUg'
    'hwYXNzd29yZA==');

@$core.Deprecated('Use prescriptionRequestDescriptor instead')
const PrescriptionRequest$json = {
  '1': 'PrescriptionRequest',
  '2': [
    {'1': 'patient_id', '3': 1, '4': 1, '5': 3, '10': 'patientId'},
    {'1': 'doctor_id', '3': 2, '4': 1, '5': 3, '10': 'doctorId'},
    {'1': 'date_of_issue', '3': 3, '4': 1, '5': 9, '10': 'dateOfIssue'},
    {'1': 'duration_days', '3': 4, '4': 1, '5': 3, '10': 'durationDays'},
    {'1': 'title', '3': 5, '4': 1, '5': 9, '10': 'title'},
    {'1': 'dosage_mg', '3': 6, '4': 1, '5': 3, '10': 'dosageMg'},
    {'1': 'way_to_use', '3': 7, '4': 1, '5': 9, '10': 'wayToUse'},
  ],
};

/// Descriptor for `PrescriptionRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List prescriptionRequestDescriptor = $convert.base64Decode(
    'ChNQcmVzY3JpcHRpb25SZXF1ZXN0Eh0KCnBhdGllbnRfaWQYASABKANSCXBhdGllbnRJZBIbCg'
    'lkb2N0b3JfaWQYAiABKANSCGRvY3RvcklkEiIKDWRhdGVfb2ZfaXNzdWUYAyABKAlSC2RhdGVP'
    'Zklzc3VlEiMKDWR1cmF0aW9uX2RheXMYBCABKANSDGR1cmF0aW9uRGF5cxIUCgV0aXRsZRgFIA'
    'EoCVIFdGl0bGUSGwoJZG9zYWdlX21nGAYgASgDUghkb3NhZ2VNZxIcCgp3YXlfdG9fdXNlGAcg'
    'ASgJUgh3YXlUb1VzZQ==');

@$core.Deprecated('Use prescriptionUpdateRequestDescriptor instead')
const PrescriptionUpdateRequest$json = {
  '1': 'PrescriptionUpdateRequest',
  '2': [
    {'1': 'id', '3': 1, '4': 1, '5': 3, '10': 'id'},
    {'1': 'patient_id', '3': 2, '4': 1, '5': 3, '9': 0, '10': 'patientId', '17': true},
    {'1': 'doctor_id', '3': 3, '4': 1, '5': 3, '9': 1, '10': 'doctorId', '17': true},
    {'1': 'date_of_issue', '3': 4, '4': 1, '5': 9, '9': 2, '10': 'dateOfIssue', '17': true},
    {'1': 'duration_days', '3': 5, '4': 1, '5': 3, '9': 3, '10': 'durationDays', '17': true},
    {'1': 'title', '3': 6, '4': 1, '5': 9, '9': 4, '10': 'title', '17': true},
    {'1': 'dosage_mg', '3': 7, '4': 1, '5': 3, '9': 5, '10': 'dosageMg', '17': true},
    {'1': 'way_to_use', '3': 8, '4': 1, '5': 9, '9': 6, '10': 'wayToUse', '17': true},
  ],
  '8': [
    {'1': '_patient_id'},
    {'1': '_doctor_id'},
    {'1': '_date_of_issue'},
    {'1': '_duration_days'},
    {'1': '_title'},
    {'1': '_dosage_mg'},
    {'1': '_way_to_use'},
  ],
};

/// Descriptor for `PrescriptionUpdateRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List prescriptionUpdateRequestDescriptor = $convert.base64Decode(
    'ChlQcmVzY3JpcHRpb25VcGRhdGVSZXF1ZXN0Eg4KAmlkGAEgASgDUgJpZBIiCgpwYXRpZW50X2'
    'lkGAIgASgDSABSCXBhdGllbnRJZIgBARIgCglkb2N0b3JfaWQYAyABKANIAVIIZG9jdG9ySWSI'
    'AQESJwoNZGF0ZV9vZl9pc3N1ZRgEIAEoCUgCUgtkYXRlT2ZJc3N1ZYgBARIoCg1kdXJhdGlvbl'
    '9kYXlzGAUgASgDSANSDGR1cmF0aW9uRGF5c4gBARIZCgV0aXRsZRgGIAEoCUgEUgV0aXRsZYgB'
    'ARIgCglkb3NhZ2VfbWcYByABKANIBVIIZG9zYWdlTWeIAQESIQoKd2F5X3RvX3VzZRgIIAEoCU'
    'gGUgh3YXlUb1VzZYgBAUINCgtfcGF0aWVudF9pZEIMCgpfZG9jdG9yX2lkQhAKDl9kYXRlX29m'
    'X2lzc3VlQhAKDl9kdXJhdGlvbl9kYXlzQggKBl90aXRsZUIMCgpfZG9zYWdlX21nQg0KC193YX'
    'lfdG9fdXNl');

@$core.Deprecated('Use patientResponseDescriptor instead')
const PatientResponse$json = {
  '1': 'PatientResponse',
  '2': [
    {'1': 'id', '3': 1, '4': 1, '5': 3, '10': 'id'},
    {'1': 'email', '3': 2, '4': 1, '5': 9, '10': 'email'},
    {'1': 'password', '3': 3, '4': 1, '5': 9, '10': 'password'},
    {'1': 'first_name', '3': 4, '4': 1, '5': 9, '10': 'firstName'},
    {'1': 'last_name', '3': 5, '4': 1, '5': 9, '10': 'lastName'},
    {'1': 'patronymic', '3': 6, '4': 1, '5': 9, '10': 'patronymic'},
    {'1': 'phone_number', '3': 7, '4': 1, '5': 9, '10': 'phoneNumber'},
  ],
};

/// Descriptor for `PatientResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List patientResponseDescriptor = $convert.base64Decode(
    'Cg9QYXRpZW50UmVzcG9uc2USDgoCaWQYASABKANSAmlkEhQKBWVtYWlsGAIgASgJUgVlbWFpbB'
    'IaCghwYXNzd29yZBgDIAEoCVIIcGFzc3dvcmQSHQoKZmlyc3RfbmFtZRgEIAEoCVIJZmlyc3RO'
    'YW1lEhsKCWxhc3RfbmFtZRgFIAEoCVIIbGFzdE5hbWUSHgoKcGF0cm9ueW1pYxgGIAEoCVIKcG'
    'F0cm9ueW1pYxIhCgxwaG9uZV9udW1iZXIYByABKAlSC3Bob25lTnVtYmVy');

@$core.Deprecated('Use patientListDescriptor instead')
const PatientList$json = {
  '1': 'PatientList',
  '2': [
    {'1': 'patients', '3': 1, '4': 3, '5': 11, '6': '.mis.PatientResponse', '10': 'patients'},
  ],
};

/// Descriptor for `PatientList`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List patientListDescriptor = $convert.base64Decode(
    'CgtQYXRpZW50TGlzdBIwCghwYXRpZW50cxgBIAMoCzIULm1pcy5QYXRpZW50UmVzcG9uc2VSCH'
    'BhdGllbnRz');

@$core.Deprecated('Use doctorResponseDescriptor instead')
const DoctorResponse$json = {
  '1': 'DoctorResponse',
  '2': [
    {'1': 'id', '3': 1, '4': 1, '5': 3, '10': 'id'},
    {'1': 'email', '3': 2, '4': 1, '5': 9, '10': 'email'},
    {'1': 'password', '3': 3, '4': 1, '5': 9, '10': 'password'},
    {'1': 'first_name', '3': 4, '4': 1, '5': 9, '10': 'firstName'},
    {'1': 'last_name', '3': 5, '4': 1, '5': 9, '10': 'lastName'},
    {'1': 'patronymic', '3': 6, '4': 1, '5': 9, '10': 'patronymic'},
    {'1': 'phone_number', '3': 7, '4': 1, '5': 9, '10': 'phoneNumber'},
    {'1': 'specialty', '3': 8, '4': 1, '5': 9, '10': 'specialty'},
    {'1': 'license', '3': 9, '4': 1, '5': 9, '10': 'license'},
  ],
};

/// Descriptor for `DoctorResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List doctorResponseDescriptor = $convert.base64Decode(
    'Cg5Eb2N0b3JSZXNwb25zZRIOCgJpZBgBIAEoA1ICaWQSFAoFZW1haWwYAiABKAlSBWVtYWlsEh'
    'oKCHBhc3N3b3JkGAMgASgJUghwYXNzd29yZBIdCgpmaXJzdF9uYW1lGAQgASgJUglmaXJzdE5h'
    'bWUSGwoJbGFzdF9uYW1lGAUgASgJUghsYXN0TmFtZRIeCgpwYXRyb255bWljGAYgASgJUgpwYX'
    'Ryb255bWljEiEKDHBob25lX251bWJlchgHIAEoCVILcGhvbmVOdW1iZXISHAoJc3BlY2lhbHR5'
    'GAggASgJUglzcGVjaWFsdHkSGAoHbGljZW5zZRgJIAEoCVIHbGljZW5zZQ==');

@$core.Deprecated('Use doctorListDescriptor instead')
const DoctorList$json = {
  '1': 'DoctorList',
  '2': [
    {'1': 'doctors', '3': 1, '4': 3, '5': 11, '6': '.mis.DoctorResponse', '10': 'doctors'},
  ],
};

/// Descriptor for `DoctorList`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List doctorListDescriptor = $convert.base64Decode(
    'CgpEb2N0b3JMaXN0Ei0KB2RvY3RvcnMYASADKAsyEy5taXMuRG9jdG9yUmVzcG9uc2VSB2RvY3'
    'RvcnM=');

@$core.Deprecated('Use prescriptionResponseDescriptor instead')
const PrescriptionResponse$json = {
  '1': 'PrescriptionResponse',
  '2': [
    {'1': 'id', '3': 1, '4': 1, '5': 3, '10': 'id'},
    {'1': 'patient_id', '3': 2, '4': 1, '5': 3, '10': 'patientId'},
    {'1': 'doctor_id', '3': 3, '4': 1, '5': 3, '10': 'doctorId'},
    {'1': 'date_of_issue', '3': 4, '4': 1, '5': 9, '10': 'dateOfIssue'},
    {'1': 'duration_days', '3': 5, '4': 1, '5': 3, '10': 'durationDays'},
    {'1': 'title', '3': 6, '4': 1, '5': 9, '10': 'title'},
    {'1': 'dosage_mg', '3': 7, '4': 1, '5': 3, '10': 'dosageMg'},
    {'1': 'way_to_use', '3': 8, '4': 1, '5': 9, '10': 'wayToUse'},
  ],
};

/// Descriptor for `PrescriptionResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List prescriptionResponseDescriptor = $convert.base64Decode(
    'ChRQcmVzY3JpcHRpb25SZXNwb25zZRIOCgJpZBgBIAEoA1ICaWQSHQoKcGF0aWVudF9pZBgCIA'
    'EoA1IJcGF0aWVudElkEhsKCWRvY3Rvcl9pZBgDIAEoA1IIZG9jdG9ySWQSIgoNZGF0ZV9vZl9p'
    'c3N1ZRgEIAEoCVILZGF0ZU9mSXNzdWUSIwoNZHVyYXRpb25fZGF5cxgFIAEoA1IMZHVyYXRpb2'
    '5EYXlzEhQKBXRpdGxlGAYgASgJUgV0aXRsZRIbCglkb3NhZ2VfbWcYByABKANSCGRvc2FnZU1n'
    'EhwKCndheV90b191c2UYCCABKAlSCHdheVRvVXNl');

@$core.Deprecated('Use prescriptionListDescriptor instead')
const PrescriptionList$json = {
  '1': 'PrescriptionList',
  '2': [
    {'1': 'prescriptions', '3': 1, '4': 3, '5': 11, '6': '.mis.PrescriptionResponse', '10': 'prescriptions'},
  ],
};

/// Descriptor for `PrescriptionList`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List prescriptionListDescriptor = $convert.base64Decode(
    'ChBQcmVzY3JpcHRpb25MaXN0Ej8KDXByZXNjcmlwdGlvbnMYASADKAsyGS5taXMuUHJlc2NyaX'
    'B0aW9uUmVzcG9uc2VSDXByZXNjcmlwdGlvbnM=');

@$core.Deprecated('Use loginRequestDescriptor instead')
const LoginRequest$json = {
  '1': 'LoginRequest',
  '2': [
    {'1': 'email', '3': 1, '4': 1, '5': 9, '10': 'email'},
    {'1': 'password', '3': 2, '4': 1, '5': 9, '10': 'password'},
  ],
};

/// Descriptor for `LoginRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List loginRequestDescriptor = $convert.base64Decode(
    'CgxMb2dpblJlcXVlc3QSFAoFZW1haWwYASABKAlSBWVtYWlsEhoKCHBhc3N3b3JkGAIgASgJUg'
    'hwYXNzd29yZA==');

@$core.Deprecated('Use loginResponseDescriptor instead')
const LoginResponse$json = {
  '1': 'LoginResponse',
  '2': [
    {'1': 'token', '3': 1, '4': 1, '5': 9, '10': 'token'},
    {'1': 'id', '3': 2, '4': 1, '5': 3, '10': 'id'},
  ],
};

/// Descriptor for `LoginResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List loginResponseDescriptor = $convert.base64Decode(
    'Cg1Mb2dpblJlc3BvbnNlEhQKBXRva2VuGAEgASgJUgV0b2tlbhIOCgJpZBgCIAEoA1ICaWQ=');

@$core.Deprecated('Use emailRequestDescriptor instead')
const EmailRequest$json = {
  '1': 'EmailRequest',
  '2': [
    {'1': 'email', '3': 1, '4': 1, '5': 9, '10': 'email'},
  ],
};

/// Descriptor for `EmailRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List emailRequestDescriptor = $convert.base64Decode(
    'CgxFbWFpbFJlcXVlc3QSFAoFZW1haWwYASABKAlSBWVtYWls');

@$core.Deprecated('Use patientDoctorIDRequestDescriptor instead')
const PatientDoctorIDRequest$json = {
  '1': 'PatientDoctorIDRequest',
  '2': [
    {'1': 'patient_id', '3': 1, '4': 1, '5': 3, '10': 'patientId'},
    {'1': 'doctor_id', '3': 2, '4': 1, '5': 3, '10': 'doctorId'},
  ],
};

/// Descriptor for `PatientDoctorIDRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List patientDoctorIDRequestDescriptor = $convert.base64Decode(
    'ChZQYXRpZW50RG9jdG9ySURSZXF1ZXN0Eh0KCnBhdGllbnRfaWQYASABKANSCXBhdGllbnRJZB'
    'IbCglkb2N0b3JfaWQYAiABKANSCGRvY3Rvcklk');

@$core.Deprecated('Use messageDescriptor instead')
const Message$json = {
  '1': 'Message',
  '2': [
    {'1': 'id', '3': 1, '4': 1, '5': 3, '9': 0, '10': 'id', '17': true},
    {'1': 'receiver_id', '3': 2, '4': 1, '5': 3, '10': 'receiverId'},
    {'1': 'sender_id', '3': 3, '4': 1, '5': 3, '10': 'senderId'},
    {'1': 'message', '3': 4, '4': 1, '5': 9, '10': 'message'},
    {'1': 'created_at', '3': 5, '4': 1, '5': 11, '6': '.google.protobuf.Timestamp', '10': 'createdAt'},
  ],
  '8': [
    {'1': '_id'},
  ],
};

/// Descriptor for `Message`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List messageDescriptor = $convert.base64Decode(
    'CgdNZXNzYWdlEhMKAmlkGAEgASgDSABSAmlkiAEBEh8KC3JlY2VpdmVyX2lkGAIgASgDUgpyZW'
    'NlaXZlcklkEhsKCXNlbmRlcl9pZBgDIAEoA1IIc2VuZGVySWQSGAoHbWVzc2FnZRgEIAEoCVIH'
    'bWVzc2FnZRI5CgpjcmVhdGVkX2F0GAUgASgLMhouZ29vZ2xlLnByb3RvYnVmLlRpbWVzdGFtcF'
    'IJY3JlYXRlZEF0QgUKA19pZA==');

@$core.Deprecated('Use joinSessionRequestDescriptor instead')
const JoinSessionRequest$json = {
  '1': 'JoinSessionRequest',
  '2': [
    {'1': 'sender_id', '3': 1, '4': 1, '5': 3, '10': 'senderId'},
    {'1': 'receiver_id', '3': 2, '4': 1, '5': 3, '10': 'receiverId'},
  ],
};

/// Descriptor for `JoinSessionRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List joinSessionRequestDescriptor = $convert.base64Decode(
    'ChJKb2luU2Vzc2lvblJlcXVlc3QSGwoJc2VuZGVyX2lkGAEgASgDUghzZW5kZXJJZBIfCgtyZW'
    'NlaXZlcl9pZBgCIAEoA1IKcmVjZWl2ZXJJZA==');

@$core.Deprecated('Use iDRequestDescriptor instead')
const IDRequest$json = {
  '1': 'IDRequest',
  '2': [
    {'1': 'id', '3': 1, '4': 1, '5': 3, '10': 'id'},
  ],
};

/// Descriptor for `IDRequest`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List iDRequestDescriptor = $convert.base64Decode(
    'CglJRFJlcXVlc3QSDgoCaWQYASABKANSAmlk');

@$core.Deprecated('Use createResponseDescriptor instead')
const CreateResponse$json = {
  '1': 'CreateResponse',
  '2': [
    {'1': 'success', '3': 1, '4': 1, '5': 8, '10': 'success'},
    {'1': 'id', '3': 2, '4': 1, '5': 3, '10': 'id'},
  ],
};

/// Descriptor for `CreateResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List createResponseDescriptor = $convert.base64Decode(
    'Cg5DcmVhdGVSZXNwb25zZRIYCgdzdWNjZXNzGAEgASgIUgdzdWNjZXNzEg4KAmlkGAIgASgDUg'
    'JpZA==');

@$core.Deprecated('Use successResponseDescriptor instead')
const SuccessResponse$json = {
  '1': 'SuccessResponse',
  '2': [
    {'1': 'success', '3': 1, '4': 1, '5': 8, '10': 'success'},
  ],
};

/// Descriptor for `SuccessResponse`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List successResponseDescriptor = $convert.base64Decode(
    'Cg9TdWNjZXNzUmVzcG9uc2USGAoHc3VjY2VzcxgBIAEoCFIHc3VjY2Vzcw==');

@$core.Deprecated('Use emptyDescriptor instead')
const Empty$json = {
  '1': 'Empty',
};

/// Descriptor for `Empty`. Decode as a `google.protobuf.DescriptorProto`.
final $typed_data.Uint8List emptyDescriptor = $convert.base64Decode(
    'CgVFbXB0eQ==');

const $core.Map<$core.String, $core.dynamic> MISServiceBase$json = {
  '1': 'MIS',
  '2': [
    {'1': 'RegisterPatient', '2': '.mis.PatientRequest', '3': '.mis.CreateResponse'},
    {'1': 'RegisterDoctor', '2': '.mis.DoctorRequest', '3': '.mis.CreateResponse'},
    {'1': 'RegisterAdmin', '2': '.mis.AdminRequest', '3': '.mis.CreateResponse'},
    {'1': 'Login', '2': '.mis.LoginRequest', '3': '.mis.LoginResponse'},
    {'1': 'GetPatientByEmail', '2': '.mis.EmailRequest', '3': '.mis.PatientResponse'},
    {'1': 'GetPatientByID', '2': '.mis.IDRequest', '3': '.mis.PatientResponse'},
    {'1': 'GetPatientPrescriptions', '2': '.mis.IDRequest', '3': '.mis.PrescriptionList'},
    {'1': 'GetPatientDoctors', '2': '.mis.IDRequest', '3': '.mis.DoctorList'},
    {'1': 'DeletePatient', '2': '.mis.IDRequest', '3': '.mis.SuccessResponse'},
    {'1': 'GetDoctorByEmail', '2': '.mis.EmailRequest', '3': '.mis.DoctorResponse'},
    {'1': 'GetDoctorByID', '2': '.mis.IDRequest', '3': '.mis.DoctorResponse'},
    {'1': 'AddPatient', '2': '.mis.EmailRequest', '3': '.mis.SuccessResponse'},
    {'1': 'GetDoctorPrescriptions', '2': '.mis.IDRequest', '3': '.mis.PrescriptionList'},
    {'1': 'GetDoctorPatients', '2': '.mis.IDRequest', '3': '.mis.PatientList'},
    {'1': 'GetDoctorPatientsWithPrescriptions', '2': '.mis.IDRequest', '3': '.mis.PatientList'},
    {'1': 'DeleteDoctor', '2': '.mis.IDRequest', '3': '.mis.SuccessResponse'},
    {'1': 'AddPrescription', '2': '.mis.PrescriptionRequest', '3': '.mis.CreateResponse'},
    {'1': 'GetPrescriptionByID', '2': '.mis.IDRequest', '3': '.mis.PrescriptionResponse'},
    {'1': 'GetPrescriptionsByBoth', '2': '.mis.PatientDoctorIDRequest', '3': '.mis.PrescriptionList'},
    {'1': 'DeletePrescription', '2': '.mis.IDRequest', '3': '.mis.SuccessResponse'},
    {'1': 'UpdatePrescription', '2': '.mis.PrescriptionUpdateRequest', '3': '.mis.SuccessResponse'},
  ],
};

@$core.Deprecated('Use mISServiceDescriptor instead')
const $core.Map<$core.String, $core.Map<$core.String, $core.dynamic>> MISServiceBase$messageJson = {
  '.mis.PatientRequest': PatientRequest$json,
  '.mis.CreateResponse': CreateResponse$json,
  '.mis.DoctorRequest': DoctorRequest$json,
  '.mis.AdminRequest': AdminRequest$json,
  '.mis.LoginRequest': LoginRequest$json,
  '.mis.LoginResponse': LoginResponse$json,
  '.mis.EmailRequest': EmailRequest$json,
  '.mis.PatientResponse': PatientResponse$json,
  '.mis.IDRequest': IDRequest$json,
  '.mis.PrescriptionList': PrescriptionList$json,
  '.mis.PrescriptionResponse': PrescriptionResponse$json,
  '.mis.DoctorList': DoctorList$json,
  '.mis.DoctorResponse': DoctorResponse$json,
  '.mis.SuccessResponse': SuccessResponse$json,
  '.mis.PatientList': PatientList$json,
  '.mis.PrescriptionRequest': PrescriptionRequest$json,
  '.mis.PatientDoctorIDRequest': PatientDoctorIDRequest$json,
  '.mis.PrescriptionUpdateRequest': PrescriptionUpdateRequest$json,
};

/// Descriptor for `MIS`. Decode as a `google.protobuf.ServiceDescriptorProto`.
final $typed_data.Uint8List mISServiceDescriptor = $convert.base64Decode(
    'CgNNSVMSOwoPUmVnaXN0ZXJQYXRpZW50EhMubWlzLlBhdGllbnRSZXF1ZXN0GhMubWlzLkNyZW'
    'F0ZVJlc3BvbnNlEjkKDlJlZ2lzdGVyRG9jdG9yEhIubWlzLkRvY3RvclJlcXVlc3QaEy5taXMu'
    'Q3JlYXRlUmVzcG9uc2USNwoNUmVnaXN0ZXJBZG1pbhIRLm1pcy5BZG1pblJlcXVlc3QaEy5taX'
    'MuQ3JlYXRlUmVzcG9uc2USLgoFTG9naW4SES5taXMuTG9naW5SZXF1ZXN0GhIubWlzLkxvZ2lu'
    'UmVzcG9uc2USPAoRR2V0UGF0aWVudEJ5RW1haWwSES5taXMuRW1haWxSZXF1ZXN0GhQubWlzLl'
    'BhdGllbnRSZXNwb25zZRI2Cg5HZXRQYXRpZW50QnlJRBIOLm1pcy5JRFJlcXVlc3QaFC5taXMu'
    'UGF0aWVudFJlc3BvbnNlEkAKF0dldFBhdGllbnRQcmVzY3JpcHRpb25zEg4ubWlzLklEUmVxdW'
    'VzdBoVLm1pcy5QcmVzY3JpcHRpb25MaXN0EjQKEUdldFBhdGllbnREb2N0b3JzEg4ubWlzLklE'
    'UmVxdWVzdBoPLm1pcy5Eb2N0b3JMaXN0EjUKDURlbGV0ZVBhdGllbnQSDi5taXMuSURSZXF1ZX'
    'N0GhQubWlzLlN1Y2Nlc3NSZXNwb25zZRI6ChBHZXREb2N0b3JCeUVtYWlsEhEubWlzLkVtYWls'
    'UmVxdWVzdBoTLm1pcy5Eb2N0b3JSZXNwb25zZRI0Cg1HZXREb2N0b3JCeUlEEg4ubWlzLklEUm'
    'VxdWVzdBoTLm1pcy5Eb2N0b3JSZXNwb25zZRI1CgpBZGRQYXRpZW50EhEubWlzLkVtYWlsUmVx'
    'dWVzdBoULm1pcy5TdWNjZXNzUmVzcG9uc2USPwoWR2V0RG9jdG9yUHJlc2NyaXB0aW9ucxIOLm'
    '1pcy5JRFJlcXVlc3QaFS5taXMuUHJlc2NyaXB0aW9uTGlzdBI1ChFHZXREb2N0b3JQYXRpZW50'
    'cxIOLm1pcy5JRFJlcXVlc3QaEC5taXMuUGF0aWVudExpc3QSRgoiR2V0RG9jdG9yUGF0aWVudH'
    'NXaXRoUHJlc2NyaXB0aW9ucxIOLm1pcy5JRFJlcXVlc3QaEC5taXMuUGF0aWVudExpc3QSNAoM'
    'RGVsZXRlRG9jdG9yEg4ubWlzLklEUmVxdWVzdBoULm1pcy5TdWNjZXNzUmVzcG9uc2USQAoPQW'
    'RkUHJlc2NyaXB0aW9uEhgubWlzLlByZXNjcmlwdGlvblJlcXVlc3QaEy5taXMuQ3JlYXRlUmVz'
    'cG9uc2USQAoTR2V0UHJlc2NyaXB0aW9uQnlJRBIOLm1pcy5JRFJlcXVlc3QaGS5taXMuUHJlc2'
    'NyaXB0aW9uUmVzcG9uc2USTAoWR2V0UHJlc2NyaXB0aW9uc0J5Qm90aBIbLm1pcy5QYXRpZW50'
    'RG9jdG9ySURSZXF1ZXN0GhUubWlzLlByZXNjcmlwdGlvbkxpc3QSOgoSRGVsZXRlUHJlc2NyaX'
    'B0aW9uEg4ubWlzLklEUmVxdWVzdBoULm1pcy5TdWNjZXNzUmVzcG9uc2USSgoSVXBkYXRlUHJl'
    'c2NyaXB0aW9uEh4ubWlzLlByZXNjcmlwdGlvblVwZGF0ZVJlcXVlc3QaFC5taXMuU3VjY2Vzc1'
    'Jlc3BvbnNl');

const $core.Map<$core.String, $core.dynamic> ChatServiceBase$json = {
  '1': 'ChatService',
  '2': [
    {'1': 'SendMessage', '2': '.mis.Message', '3': '.mis.SuccessResponse'},
    {'1': 'ReceiveMessages', '2': '.mis.JoinSessionRequest', '3': '.mis.Message', '6': true},
    {'1': 'JoinChannel', '2': '.mis.JoinSessionRequest', '3': '.mis.Message', '4': {}, '6': true},
    {'1': 'SendMsg', '2': '.mis.Message', '3': '.mis.SuccessResponse', '4': {}, '5': true},
  ],
};

@$core.Deprecated('Use chatServiceDescriptor instead')
const $core.Map<$core.String, $core.Map<$core.String, $core.dynamic>> ChatServiceBase$messageJson = {
  '.mis.Message': Message$json,
  '.google.protobuf.Timestamp': $0.Timestamp$json,
  '.mis.SuccessResponse': SuccessResponse$json,
  '.mis.JoinSessionRequest': JoinSessionRequest$json,
};

/// Descriptor for `ChatService`. Decode as a `google.protobuf.ServiceDescriptorProto`.
final $typed_data.Uint8List chatServiceDescriptor = $convert.base64Decode(
    'CgtDaGF0U2VydmljZRIxCgtTZW5kTWVzc2FnZRIMLm1pcy5NZXNzYWdlGhQubWlzLlN1Y2Nlc3'
    'NSZXNwb25zZRI6Cg9SZWNlaXZlTWVzc2FnZXMSFy5taXMuSm9pblNlc3Npb25SZXF1ZXN0Ggwu'
    'bWlzLk1lc3NhZ2UwARI4CgtKb2luQ2hhbm5lbBIXLm1pcy5Kb2luU2Vzc2lvblJlcXVlc3QaDC'
    '5taXMuTWVzc2FnZSIAMAESMQoHU2VuZE1zZxIMLm1pcy5NZXNzYWdlGhQubWlzLlN1Y2Nlc3NS'
    'ZXNwb25zZSIAKAE=');

