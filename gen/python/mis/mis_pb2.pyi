from google.protobuf import timestamp_pb2 as _timestamp_pb2
from google.protobuf.internal import containers as _containers
from google.protobuf import descriptor as _descriptor
from google.protobuf import message as _message
from typing import ClassVar as _ClassVar, Iterable as _Iterable, Mapping as _Mapping, Optional as _Optional, Union as _Union

DESCRIPTOR: _descriptor.FileDescriptor

class PatientRequest(_message.Message):
    __slots__ = ("email", "password", "first_name", "last_name", "patronymic", "phone_number")
    EMAIL_FIELD_NUMBER: _ClassVar[int]
    PASSWORD_FIELD_NUMBER: _ClassVar[int]
    FIRST_NAME_FIELD_NUMBER: _ClassVar[int]
    LAST_NAME_FIELD_NUMBER: _ClassVar[int]
    PATRONYMIC_FIELD_NUMBER: _ClassVar[int]
    PHONE_NUMBER_FIELD_NUMBER: _ClassVar[int]
    email: str
    password: str
    first_name: str
    last_name: str
    patronymic: str
    phone_number: str
    def __init__(self, email: _Optional[str] = ..., password: _Optional[str] = ..., first_name: _Optional[str] = ..., last_name: _Optional[str] = ..., patronymic: _Optional[str] = ..., phone_number: _Optional[str] = ...) -> None: ...

class DoctorRequest(_message.Message):
    __slots__ = ("email", "password", "first_name", "last_name", "patronymic", "phone_number", "specialty", "license")
    EMAIL_FIELD_NUMBER: _ClassVar[int]
    PASSWORD_FIELD_NUMBER: _ClassVar[int]
    FIRST_NAME_FIELD_NUMBER: _ClassVar[int]
    LAST_NAME_FIELD_NUMBER: _ClassVar[int]
    PATRONYMIC_FIELD_NUMBER: _ClassVar[int]
    PHONE_NUMBER_FIELD_NUMBER: _ClassVar[int]
    SPECIALTY_FIELD_NUMBER: _ClassVar[int]
    LICENSE_FIELD_NUMBER: _ClassVar[int]
    email: str
    password: str
    first_name: str
    last_name: str
    patronymic: str
    phone_number: str
    specialty: str
    license: str
    def __init__(self, email: _Optional[str] = ..., password: _Optional[str] = ..., first_name: _Optional[str] = ..., last_name: _Optional[str] = ..., patronymic: _Optional[str] = ..., phone_number: _Optional[str] = ..., specialty: _Optional[str] = ..., license: _Optional[str] = ...) -> None: ...

class AdminRequest(_message.Message):
    __slots__ = ("email", "password")
    EMAIL_FIELD_NUMBER: _ClassVar[int]
    PASSWORD_FIELD_NUMBER: _ClassVar[int]
    email: str
    password: str
    def __init__(self, email: _Optional[str] = ..., password: _Optional[str] = ...) -> None: ...

class PrescriptionRequest(_message.Message):
    __slots__ = ("patient_id", "doctor_id", "date_of_issue", "duration_days", "title", "dosage_mg", "way_to_use")
    PATIENT_ID_FIELD_NUMBER: _ClassVar[int]
    DOCTOR_ID_FIELD_NUMBER: _ClassVar[int]
    DATE_OF_ISSUE_FIELD_NUMBER: _ClassVar[int]
    DURATION_DAYS_FIELD_NUMBER: _ClassVar[int]
    TITLE_FIELD_NUMBER: _ClassVar[int]
    DOSAGE_MG_FIELD_NUMBER: _ClassVar[int]
    WAY_TO_USE_FIELD_NUMBER: _ClassVar[int]
    patient_id: int
    doctor_id: int
    date_of_issue: str
    duration_days: int
    title: str
    dosage_mg: int
    way_to_use: str
    def __init__(self, patient_id: _Optional[int] = ..., doctor_id: _Optional[int] = ..., date_of_issue: _Optional[str] = ..., duration_days: _Optional[int] = ..., title: _Optional[str] = ..., dosage_mg: _Optional[int] = ..., way_to_use: _Optional[str] = ...) -> None: ...

class PrescriptionUpdateRequest(_message.Message):
    __slots__ = ("id", "patient_id", "doctor_id", "date_of_issue", "duration_days", "title", "dosage_mg", "way_to_use")
    ID_FIELD_NUMBER: _ClassVar[int]
    PATIENT_ID_FIELD_NUMBER: _ClassVar[int]
    DOCTOR_ID_FIELD_NUMBER: _ClassVar[int]
    DATE_OF_ISSUE_FIELD_NUMBER: _ClassVar[int]
    DURATION_DAYS_FIELD_NUMBER: _ClassVar[int]
    TITLE_FIELD_NUMBER: _ClassVar[int]
    DOSAGE_MG_FIELD_NUMBER: _ClassVar[int]
    WAY_TO_USE_FIELD_NUMBER: _ClassVar[int]
    id: int
    patient_id: int
    doctor_id: int
    date_of_issue: str
    duration_days: int
    title: str
    dosage_mg: int
    way_to_use: str
    def __init__(self, id: _Optional[int] = ..., patient_id: _Optional[int] = ..., doctor_id: _Optional[int] = ..., date_of_issue: _Optional[str] = ..., duration_days: _Optional[int] = ..., title: _Optional[str] = ..., dosage_mg: _Optional[int] = ..., way_to_use: _Optional[str] = ...) -> None: ...

class PatientResponse(_message.Message):
    __slots__ = ("id", "email", "password", "first_name", "last_name", "patronymic", "phone_number")
    ID_FIELD_NUMBER: _ClassVar[int]
    EMAIL_FIELD_NUMBER: _ClassVar[int]
    PASSWORD_FIELD_NUMBER: _ClassVar[int]
    FIRST_NAME_FIELD_NUMBER: _ClassVar[int]
    LAST_NAME_FIELD_NUMBER: _ClassVar[int]
    PATRONYMIC_FIELD_NUMBER: _ClassVar[int]
    PHONE_NUMBER_FIELD_NUMBER: _ClassVar[int]
    id: int
    email: str
    password: str
    first_name: str
    last_name: str
    patronymic: str
    phone_number: str
    def __init__(self, id: _Optional[int] = ..., email: _Optional[str] = ..., password: _Optional[str] = ..., first_name: _Optional[str] = ..., last_name: _Optional[str] = ..., patronymic: _Optional[str] = ..., phone_number: _Optional[str] = ...) -> None: ...

class PatientList(_message.Message):
    __slots__ = ("patients",)
    PATIENTS_FIELD_NUMBER: _ClassVar[int]
    patients: _containers.RepeatedCompositeFieldContainer[PatientResponse]
    def __init__(self, patients: _Optional[_Iterable[_Union[PatientResponse, _Mapping]]] = ...) -> None: ...

class DoctorResponse(_message.Message):
    __slots__ = ("id", "email", "password", "first_name", "last_name", "patronymic", "phone_number", "specialty", "license")
    ID_FIELD_NUMBER: _ClassVar[int]
    EMAIL_FIELD_NUMBER: _ClassVar[int]
    PASSWORD_FIELD_NUMBER: _ClassVar[int]
    FIRST_NAME_FIELD_NUMBER: _ClassVar[int]
    LAST_NAME_FIELD_NUMBER: _ClassVar[int]
    PATRONYMIC_FIELD_NUMBER: _ClassVar[int]
    PHONE_NUMBER_FIELD_NUMBER: _ClassVar[int]
    SPECIALTY_FIELD_NUMBER: _ClassVar[int]
    LICENSE_FIELD_NUMBER: _ClassVar[int]
    id: int
    email: str
    password: str
    first_name: str
    last_name: str
    patronymic: str
    phone_number: str
    specialty: str
    license: str
    def __init__(self, id: _Optional[int] = ..., email: _Optional[str] = ..., password: _Optional[str] = ..., first_name: _Optional[str] = ..., last_name: _Optional[str] = ..., patronymic: _Optional[str] = ..., phone_number: _Optional[str] = ..., specialty: _Optional[str] = ..., license: _Optional[str] = ...) -> None: ...

class DoctorList(_message.Message):
    __slots__ = ("doctors",)
    DOCTORS_FIELD_NUMBER: _ClassVar[int]
    doctors: _containers.RepeatedCompositeFieldContainer[DoctorResponse]
    def __init__(self, doctors: _Optional[_Iterable[_Union[DoctorResponse, _Mapping]]] = ...) -> None: ...

class PrescriptionResponse(_message.Message):
    __slots__ = ("id", "patient_id", "doctor_id", "date_of_issue", "duration_days", "title", "dosage_mg", "way_to_use")
    ID_FIELD_NUMBER: _ClassVar[int]
    PATIENT_ID_FIELD_NUMBER: _ClassVar[int]
    DOCTOR_ID_FIELD_NUMBER: _ClassVar[int]
    DATE_OF_ISSUE_FIELD_NUMBER: _ClassVar[int]
    DURATION_DAYS_FIELD_NUMBER: _ClassVar[int]
    TITLE_FIELD_NUMBER: _ClassVar[int]
    DOSAGE_MG_FIELD_NUMBER: _ClassVar[int]
    WAY_TO_USE_FIELD_NUMBER: _ClassVar[int]
    id: int
    patient_id: int
    doctor_id: int
    date_of_issue: str
    duration_days: int
    title: str
    dosage_mg: int
    way_to_use: str
    def __init__(self, id: _Optional[int] = ..., patient_id: _Optional[int] = ..., doctor_id: _Optional[int] = ..., date_of_issue: _Optional[str] = ..., duration_days: _Optional[int] = ..., title: _Optional[str] = ..., dosage_mg: _Optional[int] = ..., way_to_use: _Optional[str] = ...) -> None: ...

class PrescriptionList(_message.Message):
    __slots__ = ("prescriptions",)
    PRESCRIPTIONS_FIELD_NUMBER: _ClassVar[int]
    prescriptions: _containers.RepeatedCompositeFieldContainer[PrescriptionResponse]
    def __init__(self, prescriptions: _Optional[_Iterable[_Union[PrescriptionResponse, _Mapping]]] = ...) -> None: ...

class LoginRequest(_message.Message):
    __slots__ = ("email", "password")
    EMAIL_FIELD_NUMBER: _ClassVar[int]
    PASSWORD_FIELD_NUMBER: _ClassVar[int]
    email: str
    password: str
    def __init__(self, email: _Optional[str] = ..., password: _Optional[str] = ...) -> None: ...

class LoginResponse(_message.Message):
    __slots__ = ("token", "id")
    TOKEN_FIELD_NUMBER: _ClassVar[int]
    ID_FIELD_NUMBER: _ClassVar[int]
    token: str
    id: int
    def __init__(self, token: _Optional[str] = ..., id: _Optional[int] = ...) -> None: ...

class EmailRequest(_message.Message):
    __slots__ = ("email",)
    EMAIL_FIELD_NUMBER: _ClassVar[int]
    email: str
    def __init__(self, email: _Optional[str] = ...) -> None: ...

class PatientDoctorIDRequest(_message.Message):
    __slots__ = ("patient_id", "doctor_id")
    PATIENT_ID_FIELD_NUMBER: _ClassVar[int]
    DOCTOR_ID_FIELD_NUMBER: _ClassVar[int]
    patient_id: int
    doctor_id: int
    def __init__(self, patient_id: _Optional[int] = ..., doctor_id: _Optional[int] = ...) -> None: ...

class Message(_message.Message):
    __slots__ = ("id", "receiver_id", "sender_id", "message", "created_at")
    ID_FIELD_NUMBER: _ClassVar[int]
    RECEIVER_ID_FIELD_NUMBER: _ClassVar[int]
    SENDER_ID_FIELD_NUMBER: _ClassVar[int]
    MESSAGE_FIELD_NUMBER: _ClassVar[int]
    CREATED_AT_FIELD_NUMBER: _ClassVar[int]
    id: int
    receiver_id: int
    sender_id: int
    message: str
    created_at: _timestamp_pb2.Timestamp
    def __init__(self, id: _Optional[int] = ..., receiver_id: _Optional[int] = ..., sender_id: _Optional[int] = ..., message: _Optional[str] = ..., created_at: _Optional[_Union[_timestamp_pb2.Timestamp, _Mapping]] = ...) -> None: ...

class JoinSessionRequest(_message.Message):
    __slots__ = ("sender_id", "receiver_id")
    SENDER_ID_FIELD_NUMBER: _ClassVar[int]
    RECEIVER_ID_FIELD_NUMBER: _ClassVar[int]
    sender_id: int
    receiver_id: int
    def __init__(self, sender_id: _Optional[int] = ..., receiver_id: _Optional[int] = ...) -> None: ...

class IDRequest(_message.Message):
    __slots__ = ("id",)
    ID_FIELD_NUMBER: _ClassVar[int]
    id: int
    def __init__(self, id: _Optional[int] = ...) -> None: ...

class CreateResponse(_message.Message):
    __slots__ = ("success", "id")
    SUCCESS_FIELD_NUMBER: _ClassVar[int]
    ID_FIELD_NUMBER: _ClassVar[int]
    success: bool
    id: int
    def __init__(self, success: bool = ..., id: _Optional[int] = ...) -> None: ...

class SuccessResponse(_message.Message):
    __slots__ = ("success",)
    SUCCESS_FIELD_NUMBER: _ClassVar[int]
    success: bool
    def __init__(self, success: bool = ...) -> None: ...

class Empty(_message.Message):
    __slots__ = ()
    def __init__(self) -> None: ...
