# protos



## Getting started
Генерация
* Go auth
`protoc -I proto proto/auth/auth.proto --go_out=./gen/go --go_opt=paths=source_relative --go-grpc_out=./gen/go --go-grpc_opt=paths=source_relative`
* Go mis
`protoc -I proto proto/mis/mis.proto --go_out=./gen/go --go_opt=paths=source_relative --go-grpc_out=./gen/go --go-grpc_opt=paths=source_relative`
* Py mis
`python -m grpc_tools.protoc --proto_path=./proto/mis --python_out=./gen/python/mis --grpc_python_out=./gen/python/mis --pyi_out=./gen/python/mis ./proto/mis/mis.proto`
* Dart mis
1. `dart pub global activate protoc_plugin`
2. `export PATH="$PATH:$HOME/.pub-cache/bin"`
3. `protoc --proto_path=proto --dart_out=gen/dart proto/mis/mis.proto`

## Go
* `go install google.golang.org/grpc/cmd/protoc-gen-go-grpc@v1.2`
* `go install google.golang.org/protobuf/cmd/protoc-gen-go@v1.28`
* `export PATH="$PATH:$(go env GOPATH)/bin"`
* `export PATH=$PATH:$HOME/.local/bin`